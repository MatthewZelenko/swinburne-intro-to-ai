from point2d import Point2D
from graphics import *

class Agent(object):
    
    def __init__(self, a_x, a_y):
        self.position = Point2D(a_x, a_y)
        self.path = None
        self.currentIndex = 0
        self.lerpValue = 0
        self.speed = 4

    def Move(self, path):
        self.path = path
        if not self.path:
            return
        self.position = self.path[0]._vc
        self.currentIndex = 0
        self.lerpValue = 0

    def Update(self, dt):
        if not self.path:
            return

        if self.currentIndex + 1 >= len(self.path):
            self.path = None
            print("Reached Destination")
            return

        startPosition = self.path[self.currentIndex]._vc
        endPosition = self.path[self.currentIndex + 1]._vc
        
        self.lerpValue += dt * self.speed

        toEndPosition = endPosition.to(startPosition)
        diff = toEndPosition.magnitude()
        toEndPosition.normalize()

        if self.lerpValue > 1:
            self.lerpValue = 1
        if self.lerpValue < 0:
            self.lerpValue = 0

        self.position = startPosition + (toEndPosition * (diff * self.lerpValue))
        if self.position == endPosition:
            self.currentIndex += 1
            self.lerpValue = 0


    def draw(self):
        egi.set_pen_color(name="GREEN")
        egi.circle(self.position, 20)