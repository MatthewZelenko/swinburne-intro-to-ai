'''Autonomous Agent Movement: Seek, Arrive and Flee

Created for COS30002 AI for Games, Lab 05
By Clinton Woodward cwoodward@swin.edu.au

'''
from graphics import egi, KEY
from pyglet import window, clock
from pyglet.gl import *

from vector2d import Vector2D
from world import World
from agent import Agent 
from target import Target
from random import randrange, uniform

def on_mouse_press(x, y, button, modifiers):
    pass;


def on_key_press(symbol, modifiers):
    if symbol == KEY.P:
        world.paused = not world.paused
    elif symbol in Agent.WEAPONS:
        for agent in world.agents:
            agent.weapon = Agent.WEAPONS[symbol]
    # Toggle debug force line info on the agent
    elif symbol == KEY.I:
        for agent in world.agents:
            agent.show_info = not agent.show_info

    elif symbol == KEY.T:
        r = randrange(5, 20)
        i = randrange(0, 8)
        if i == 0:
            minPoint = Vector2D(r, r)
            maxPoint = Vector2D(world.cx - r, r)
        elif i == 1:
            minPoint = Vector2D(world.cx - r, r)
            maxPoint = Vector2D(r, r)
        elif i == 2:
            minPoint = Vector2D(r, r)
            maxPoint = Vector2D(r, world.cy - r)
        elif i == 3:
            minPoint = Vector2D(r, world.cy - r)
            maxPoint = Vector2D(r, r)
        elif i == 4:
            minPoint = Vector2D(world.cx - r, world.cy - r)
            maxPoint = Vector2D(world.cx - r, r)
        elif i == 5:
            minPoint = Vector2D(world.cx - r, r)
            maxPoint = Vector2D(world.cx - r, world.cy - r)
        elif i == 6:
            minPoint = Vector2D(world.cx - r, world.cy - r)
            maxPoint = Vector2D(r, world.cy - r)
        elif i == 7:
            minPoint = Vector2D(r, world.cy - r)
            maxPoint = Vector2D(world.cx - r, world.cy - r)

        world.targets.append(Target(minPoint, maxPoint, r, uniform(1, 2)))

def on_resize(cx, cy):
    pass;


if __name__ == '__main__':

    # create a pyglet window and set glOptions
    win = window.Window(width=500, height=500, vsync=True, resizable=True)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # needed so that egi knows where to draw
    egi.InitWithPyglet(win)
    # prep the fps display
    fps_display = clock.ClockDisplay()
    # register key and mouse event handlers
    win.push_handlers(on_key_press)
    win.push_handlers(on_mouse_press)
    win.push_handlers(on_resize)

    # create a world for agents
    world = World(500, 500)
    # unpause the world ready for movement
    world.paused = False

    #Attacker
    world.agents.append(Agent(len(world.agents), world))

    while not win.has_exit:
        win.dispatch_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # show nice FPS bottom right (default)
        delta = clock.tick()
        world.update(delta)
        world.render()
        fps_display.draw()
        # swap the double buffer
        win.flip()