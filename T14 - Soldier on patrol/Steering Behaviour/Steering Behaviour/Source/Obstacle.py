from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY

class Obstacle(object):
    def __init__(self, cx, cy, radius):
        self.pos = Vector2D(cx, cy)
        self.radius = radius

    def render(self):        
        egi.grey_pen()
        egi.circle(self.pos, self.radius, filled=True)