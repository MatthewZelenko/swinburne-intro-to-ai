''' 2D Point

Created for COS3002 AI for Games by Clinton Woodward cwoodward@swin.edu.au
Please don't share without permission.

'''
from math import sqrt

class Point2D(object):

    __slots__ = ('x','y')

    def __init__(self, x=0.0, y=0.0):
        self.x = x
        self.y = y

    def copy(self):
        return Point2D(self.x, self.y)

    def __str__(self):
        return '(%5.2f,%5.2f)' % (self.x, self.y)
    
    def to(self, otherPoint):
        dx = self.x - otherPoint.x
        dy = self.y - otherPoint.y
        return  Point2D(dx, dy)

    def magnitude_sq(self):
        return self.x * self.x + self.y * self.y

    def magnitude(self):
        return sqrt(self.magnitude_sq())

    def normalize(self):
        distance = self.magnitude_sq()
        xSign = -1 if self.x < 0 else 1
        ySign = -1 if self.y < 0 else 1
        self.x = (self.x * self.x) / distance * xSign
        self.y = (self.y * self.y) / distance * ySign 

    def __add__(self, other):
        return Point2D(self.x + other.x, self.y + other.y)

    def __mul__(self, other):
        return Point2D(self.x * other, self.y * other)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y