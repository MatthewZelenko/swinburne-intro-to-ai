from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path


class Target(object):

    def __init__(self, minPos, maxPos, size, speed = 2.0):
        self.size = size
        self.minPos = minPos
        self.maxPos = maxPos
        self.speed = speed

        self.dir = self.maxPos - self.minPos
        self.lenOfPoints = self.dir.length()
        self.dir.normalise()
        self.pos = self.minPos + self.dir * uniform(0, self.lenOfPoints)
        self.movingForward = True
        self.is_destroyed = False;

    def update(self, delta):
        self.pos += self.dir * self.speed
        if self.movingForward:
            if (self.pos - self.maxPos).length() <= self.speed:
                self.movingForward = False;
                self.pos = self.maxPos.copy()
                self.dir = -self.dir
        else:
            if (self.pos - self.minPos).length() <= self.speed:
                self.movingForward = True
                self.pos = self.minPos.copy()
                self.dir = -self.dir
        
    def render(self):
        egi.red_pen()
        egi.circle(self.pos, self.size, filled=True)
        egi.circle(self.pos, self.size * 0.5, filled=True)
        egi.circle(self.pos, self.size * 0.25, filled=True)