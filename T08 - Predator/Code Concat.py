#---------------------------------AGENT------------------------------------
'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward cwoodward@swin.edu.au

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path

AGENT_MODES = {
    KEY._1: 'hunt', #Uses wander and pursuit
    KEY._2: 'hide', #Uses arrive
}

class Agent(object):
    def __init__(self, id=0, world=None, scale=5.0, mass=1.0, mode='hide', hunter=None):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size

        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass
        
        self.id = id

        self.color = (uniform(0, 1), uniform(0, 1), uniform(0, 1), 1.0)

        # data for drawing this agent
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]

        ### wander details
        self.wander_target = Vector2D(1, 0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale
        self.wander_jitter = 5.0 * scale
        self.bRadius = scale

        #hide
        self.hunter = hunter
        
        #hunter
        self.hideSpots = []
        self.hideSpotsColours = []

        # limits?
        self.max_speed = 20.0 * scale
        ## max_force ??
        self.max_force = 500.0

        #arrive
        self.arriveDrag = 0.1
        self.arriveRadius = scale * 10.0

        # debug draw info?
        self.show_info = False


    def calculate(self, delta):
        # calculate the current steering force
        mode = self.mode
        if mode == 'hunt':
            force = self.hunt(delta)
        elif mode == 'hide':
            force = self.hide(delta)
        else:
            force = Vector2D()
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        ## force = self.calculate()
        force = self.calculate(delta)  # <-- delta needed for wander
        ## limit force? <-- for wander
        force.truncate(self.max_force)
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

        self.hideSpots = []
        self.hideSpotsColours = []
        
        for c in self.world.circles:
            dir = (c.pos - self.pos).normalise()
            self.hideSpots.append(c.pos + (dir * (c.radius + 10)))
            self.hideSpotsColours.append((0.0, 1.0, 0.0, 1.0))

    def render(self, color=None):
        # draw the ship
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        if self.mode == 'hunt':
            egi.red_pen()
        else:
            egi.set_pen_color(self.color)
        egi.closed_shape(pts)

        # draw wander info?
        if self.mode == 'wander':
            #calculate the center of the wander circle in fron of the agent
            wnd_pos = Vector2D(self.wander_dist, 0)
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)

            #draw the wander circle
            egi.green_pen()
            egi.circle(wld_pos, self.wander_radius)

            # draw the wander target
            egi.red_pen()
            wnd_pos = (self.wander_target + Vector2D(self.wander_dist, 0))
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            egi.circle(wld_pos, 3)
            pass

        if self.mode == 'hunt':
            for i in range(len(self.hideSpots)):
                egi.orange_pen()
                egi.line(pos1=self.pos, pos2=self.hideSpots[i])
                egi.set_pen_color(self.hideSpotsColours[i])
                egi.cross(self.hideSpots[i], 5)

        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

    def speed(self):
        return self.vel.length()

    #-------------------------------MOVEMENT-------------------------------
    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        wt = self.wander_target
        jitter_tts = self.wander_jitter * delta
        wt += Vector2D(uniform(-1, 1) * jitter_tts, uniform(-1, 1) * jitter_tts)
        wt.normalise()
        wt *= self.wander_radius
        target = wt + Vector2D(self.wander_dist, 0)
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)

        desiredVel = (wld_target - self.pos).get_normalised() * self.max_speed

        return desiredVel - self.vel

    def arrive(self, target_pos):
        ''' this behaviour is similar to seek() but it attempts to arrive at
            the target position with a zero velocity'''
        decel_rate = 0.5
        to_target = target_pos - self.pos
        dist = to_target.length()
        if dist > 0:
            # calculate the speed required to reach the target given the
            # desired deceleration rate
            speed = dist / decel_rate
            # make sure the velocity does not exceed the max
            speed = min(speed, self.max_speed)
            # from here proceed just like Seek except we don't need to
            # normalize the to_target vector because we have already gone to the
            # trouble of calculating its length for dist.
            desired_vel = to_target * (speed / dist)
            return (desired_vel - self.vel)
        return Vector2D(0, 0)

    def pursuit(self, evader):
        ''' this behaviour predicts where an agent will be in time T and seeks
            towards that point to intercept it. '''
        ## OPTIONAL EXTRA... pursuit (you'll need something to pursue!)
        return Vector2D()

    #-------------------MODES-------------------------
    def hunt(self, delta):
        return self.wander(delta)

    def hide(self, delta):
        if not self.hunter:
            return Vector2D()
            #TODO if too close to hunter
        #grab hunters hide locations
        hideSpots = self.hunter.hideSpots

        closestSpot = hideSpots[0]
        closestDistance = (hideSpots[0] - self.pos).length()
        closestIndex = 0

        for i in range(1, len(hideSpots)):
            spotDistance = (hideSpots[i] - self.pos).length()
            if spotDistance < closestDistance:
                closestDistance = spotDistance
                closestSpot = hideSpots[i]
                closestIndex = i

        self.hunter.hideSpotsColours[closestIndex] = self.color
        return self.arrive(closestSpot)
#---------------------------------AGENT------------------------------------





#---------------------------------MAIN------------------------------------
'''Autonomous Agent Movement: Seek, Arrive and Flee

Created for COS30002 AI for Games, Lab 05
By Clinton Woodward cwoodward@swin.edu.au

'''
from graphics import egi, KEY
from pyglet import window, clock
from pyglet.gl import *

from vector2d import Vector2D
from world import World
from agent import Agent, AGENT_MODES  # Agent with seek, arrive, flee and pursuit


def on_mouse_press(x, y, button, modifiers):
    pass;


def on_key_press(symbol, modifiers):
    if symbol == KEY.P:
        world.paused = not world.paused
    elif symbol in AGENT_MODES:
        for agent in world.agents:
            agent.mode = AGENT_MODES[symbol]
    elif symbol == KEY.A:
        world.agents.append(Agent(len(world.agents), world, hunter=world.agents[0]))
    elif symbol == KEY.R:
        world.CreateLevel(10)
    # Toggle debug force line info on the agent
    elif symbol == KEY.I:
        for agent in world.agents:
            agent.show_info = not agent.show_info

def on_resize(cx, cy):
    pass;


if __name__ == '__main__':

    # create a pyglet window and set glOptions
    win = window.Window(width=500, height=500, vsync=True, resizable=True)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # needed so that egi knows where to draw
    egi.InitWithPyglet(win)
    # prep the fps display
    fps_display = clock.ClockDisplay()
    # register key and mouse event handlers
    win.push_handlers(on_key_press)
    win.push_handlers(on_mouse_press)
    win.push_handlers(on_resize)

    # create a world for agents
    world = World(500, 500)
    # unpause the world ready for movement
    world.paused = False
    world.agents.append(Agent(len(world.agents), world, mode='hunt'))

    while not win.has_exit:
        win.dispatch_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # show nice FPS bottom right (default)
        delta = clock.tick()
        world.update(delta)
        world.render()
        fps_display.draw()
        # swap the double buffer
        win.flip()
#---------------------------------MAIN------------------------------------





#---------------------------------OBSTACLE------------------------------------
from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY

class Obstacle(object):
    def __init__(self, cx, cy, radius):
        self.pos = Vector2D(cx, cy)
        self.radius = radius

    def render(self):        
        egi.grey_pen()
        egi.circle(self.pos, self.radius, filled=True)
#---------------------------------OBSTACLE------------------------------------





#---------------------------------WORLD------------------------------------
'''A 2d world that supports agents with steering behaviour

Created for COS30002 AI for Games by Clinton Woodward cwoodward@swin.edu.au

'''

from vector2d import Vector2D
from matrix33 import Matrix33
from graphics import egi
from random import random, randrange, uniform
from Obstacle import Obstacle

class World(object):
    def __init__(self, cx, cy):
        self.cx = cx
        self.cy = cy
        self.target = Vector2D(cx / 2, cy / 2)
        self.agents = []
        self.paused = True
        self.show_info = True
        self.CreateLevel(10)

    def update(self, delta):
        if not self.paused:
            for agent in self.agents:
                agent.update(delta)

    def render(self):
        for agent in self.agents:
            agent.render()

        for obs in self.circles:
            obs.render()

        if self.target:
            egi.red_pen()
            egi.cross(self.target, 10)

        if self.show_info:
            infotext = ', '.join(set(agent.mode for agent in self.agents))
            egi.white_pen()
            egi.text_at_pos(0, 0, infotext)


    def wrap_around(self, pos):
        ''' Treat world as a toroidal space. Updates parameter object pos '''
        max_x, max_y = self.cx, self.cy
        if pos.x > max_x:
            pos.x = pos.x - max_x
        elif pos.x < 0:
            pos.x = max_x - pos.x
        if pos.y > max_y:
            pos.y = pos.y - max_y
        elif pos.y < 0:
            pos.y = max_y - pos.y

    def transform_points(self, points, pos, forward, side, scale):
        ''' Transform the given list of points, using the provided position,
            direction and scale, to object world space. '''
        # make a copy of original points (so we don't trash them)
        wld_pts = [pt.copy() for pt in points]
        # create a transformation matrix to perform the operations
        mat = Matrix33()
        # scale,
        mat.scale_update(scale.x, scale.y)
        # rotate
        mat.rotate_by_vectors_update(forward, side)
        # and translate
        mat.translate_update(pos.x, pos.y)
        # now transform all the points (vertices)
        mat.transform_vector2d_list(wld_pts)
        # done
        return wld_pts
    
    def transform_point(self, point, pos, forward, side):
        ''' Transform the given single point, using the provided position, and direction (forward and side unit vectors), to object world space. '''
        # make a copy of the original point (so we don't trash it)
        wld_pt = point.copy()
        # create a transformation matrix to perform the operations
        mat = Matrix33()
        # rotate
        mat.rotate_by_vectors_update(forward, side)
        # and translate
        mat.translate_update(pos.x, pos.y)
        # now transform the point (in place)
        mat.transform_vector2d(wld_pt)
        # done
        return wld_pt

    def CreateLevel(self, numOfCircles):
        self.circles = []
        for x in range(numOfCircles):
            r = randrange(10, 40)
            x = randrange(self.cx - (r + r))
            y = randrange(self.cy - (r + r))
            self.circles.append(Obstacle(x, y, r))

        count = 0
        success = False
        while (count < 20 and success == False):
            success = True
            for circleI in self.circles:
                for circleJ in self.circles:
                    if circleI == circleJ:
                        continue;
                    diff = (circleI.pos - circleJ.pos)
                    if diff.length() < circleI.radius + circleJ.radius:
                        #push out
                        circleI.pos += diff * 0.5
                        circleJ.pos -= diff * 0.5
                        success = False
#---------------------------------WORLD------------------------------------