﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxController : MonoBehaviour
{
    public float m_speed;
    public bool m_doubleSpeed = false;
    public List<Entity> m_entities;
    public List<float> m_entityParallaxValues;
    void Start()
    {

    }
    void Update()
    {
        Move();
    }

    void Move()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
            m_doubleSpeed = true;
        else if (Input.GetKeyUp(KeyCode.LeftShift))
            m_doubleSpeed = false;


        float movement = 0;

        if (Input.GetKey(KeyCode.A))
            movement -= m_speed * Time.unscaledDeltaTime;
        if (Input.GetKey(KeyCode.D))
            movement += m_speed * Time.unscaledDeltaTime;

        if (movement != 0)
        {
            if (m_doubleSpeed)
                movement *= 2;

            transform.Translate(movement, 0, 0);

            for (int i = 0; i < m_entities.Count; i++)
            {
                m_entities[i].transform.Translate(movement * m_entityParallaxValues[i], 0, 0);
            }
        }
    }
}