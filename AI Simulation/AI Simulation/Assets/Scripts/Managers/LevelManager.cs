﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public enum LevelLayerTypes
    {
        BG,
        FOREST,
        BUILDING,
        VILLAGER,
        FG,
        PLATFORM
    }
    public List<LevelLayer> m_layers;

    public int m_minTreeCount, m_maxTreeCount;

    public int m_tileSize;
    public int m_tilesCount;
    StaticEntity[] m_tiles;
    int m_middleTile;


    public LevelManager()
    {
    }

    public T GetContainer<T>(LevelLayerTypes a_type) where T : LevelLayer
    {
        return m_layers[(int)a_type] as T;
    }

    public void CreateLevel()
    {
        m_tiles = new StaticEntity[m_tilesCount];
        m_middleTile = m_tilesCount / 2;
        for (int i = 0; i < m_layers.Count; i++)
        {
            m_layers[i].m_spriteOrder = i - m_layers.Count;
            m_layers[i].transform.position = new Vector3(m_layers[i].transform.position.x, m_layers[i].transform.position.y, m_layers[i].m_spriteOrder * -10);
            m_layers[i].Init();
        }
        List<Villager> villagers = CreateVillagers();
        CreateHouses(villagers);
        CreateTrees();
    }

    public Villager CreateVillager(Villager a_villager)
    {
        m_layers[(int)LevelLayerTypes.VILLAGER].AddEntity(a_villager);
        a_villager.transform.Translate(new Vector3(UnityEngine.Random.Range(-100, 100), 0, 0));
        return a_villager;
    }
    List<Villager> CreateVillagers()
    {
        VillagerFactory mgr = GameManager.GetInstance().GetFactory<VillagerFactory>();

        List<Villager> villagers = new List<Villager>
        {
            CreateVillager(mgr.CreateVillager(Villager.JobType.Builder)),
            CreateVillager(mgr.CreateVillager(Villager.JobType.Lumberjack)),
            CreateVillager(mgr.CreateVillager(Villager.JobType.Lumberjack)),
            CreateVillager(mgr.CreateVillager(Villager.JobType.Farmer)),
            CreateVillager(mgr.CreateVillager(Villager.JobType.Farmer)),
            CreateVillager(mgr.CreateVillager(Villager.JobType.Farmer))
        };
        return villagers;
    }
    void CreateTrees()
    {
        ForestFactory mgr = GameManager.GetInstance().GetFactory<ForestFactory>();

        int treeCount = UnityEngine.Random.Range(m_minTreeCount, m_maxTreeCount + 1);
        for (int i = 0; i < treeCount; i++)
        {
            Tree tree = mgr.CreateTree();
            m_layers[(int)LevelLayerTypes.FOREST].AddEntity(tree);
            AddRightEntity(tree);
        }
    }
    void CreateHouses(List<Villager> a_villagers)
    {
        HouseFactory mgr = GameManager.GetInstance().GetFactory<HouseFactory>();
        //Warehouse
        Warehouse warehouse = mgr.CreateWarehouse();
        m_layers[(int)LevelLayerTypes.BUILDING].AddEntity(warehouse);
        AddRightEntity(warehouse);


        //Houses
        int houseCount = a_villagers.Count / House.MAX_NUM_OF_RESIDENTS;

        for (int i = 0; i < houseCount; i++)
        {
            House house = mgr.CreateHouse(true);
            m_layers[(int)LevelLayerTypes.BUILDING].AddEntity(house);
            AddLeftEntity(house);

            //Add residents
            for (int j = i * House.MAX_NUM_OF_RESIDENTS; j < a_villagers.Count; j++)
            {
                if (house.m_residentComponent.Full)
                    break;

                house.m_residentComponent.AddResident(a_villagers[j]);
            }
        }
    }
    public bool AddLeftEntity(StaticEntity a_entity)
    {
        List<int> tiles = new List<int>();
        for (int i = m_middleTile; i >= 0; i--)
        {
            if (!m_tiles[i])
            {
                tiles.Add(i);
                if (tiles.Count == a_entity.TileSize)
                {
                    tiles.Reverse();
                    for (int j = 0; j < tiles.Count; j++)
                    {
                        m_tiles[tiles[j]] = a_entity;
                    }
                    a_entity.transform.localPosition = new Vector3(tiles[0] * m_tileSize - (m_tilesCount * m_tileSize * 0.5f), a_entity.transform.localPosition.y, a_entity.transform.localPosition.z);
                    a_entity.OccupiedTiles = tiles;
                    return true;
                }
            }
            else
            {
                tiles.Clear();
            }
        }
        return false;
    }
    public bool AddRightEntity(StaticEntity a_entity)
    {
        List<int> tiles = new List<int>();
        for (int i = m_middleTile; i < m_tilesCount; i++)
        {
            if (!m_tiles[i])
            {
                tiles.Add(i);
                if (tiles.Count == a_entity.TileSize)
                {
                    for (int j = 0; j < tiles.Count; j++)
                    {
                        m_tiles[tiles[j]] = a_entity;
                    }
                    a_entity.transform.localPosition = new Vector3(tiles[0] * m_tileSize - (m_tilesCount * m_tileSize * 0.5f), a_entity.transform.localPosition.y, a_entity.transform.localPosition.z);
                    a_entity.OccupiedTiles = tiles;
                    return true;
                }
            }
            else
            {
                tiles.Clear();
            }
        }
        return false;
    }
}