﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class LevelLayer : MonoBehaviour
{
    [HideInInspector]
    public int m_spriteOrder;
    [HideInInspector]
    public List<Entity> m_entities;
    public virtual void Init()
    {

    }
    void Awake()
    {
        m_entities = new List<Entity>();
    }
    void FixedUpdate()
    {
        for (int i = m_entities.Count - 1; i >= 0; i--)
        {
            if (m_entities[i].Dead)
            {
                m_entities.RemoveAt(i);
            }
        }
    }

    public void AddEntity(Entity a_entity)
    {
        a_entity.transform.parent = transform;
        a_entity.m_spriteRenderer.sortingOrder = m_spriteOrder;
        a_entity.transform.localPosition = new Vector3(a_entity.transform.localPosition.x, a_entity.transform.localPosition.y, 0);
        m_entities.Add(a_entity);
    }
}