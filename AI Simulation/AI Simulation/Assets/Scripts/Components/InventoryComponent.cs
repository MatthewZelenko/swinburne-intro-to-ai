﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class InventoryComponent : Component
{
    public Dictionary<Resource.ResourceType, int> m_resources;
    int totalResourceCount;

    public InventoryComponent(Entity a_entity) : base(a_entity)
    {
        m_resources = new Dictionary<Resource.ResourceType, int>();
        totalResourceCount = 0;
    }

    public override void Init()
    {

    }
    public override void Update()
    {

    }
    public override void FixedUpdate()
    {

    }

    public void WriteToConsole()
    {
        var enumerator = m_resources.GetEnumerator();
        string debugStr = "Warehouse: \n";
        while (enumerator.MoveNext())
        {
            debugStr += "\t" + enumerator.Current.Key + ": " + enumerator.Current.Value + "\n";
        }
        Debug.Log(debugStr);
    }

    public override void Clear()
    {

    }

    public void AddResource(Resource a_resource)
    {
        if (a_resource == null)
            return;
        if (!m_resources.ContainsKey(a_resource.TypeOfResource))
        {
            m_resources.Add(a_resource.TypeOfResource, 0);
        }
        m_resources[a_resource.TypeOfResource] += a_resource.Count;
        totalResourceCount += a_resource.Count;
    }
    public void AddAllResources(List<Resource> a_resources)
    {
        if (a_resources == null || a_resources.Count == 0)
            return;
        for (int i = 0; i < a_resources.Count; i++)
        {
            if (a_resources[i] == null || a_resources[i].Count == 0)
                continue;

            if (!m_resources.ContainsKey(a_resources[i].TypeOfResource))
            {
                m_resources.Add(a_resources[i].TypeOfResource, a_resources[i].Count);
            }
            else
            {
                m_resources[a_resources[i].TypeOfResource] += a_resources[i].Count;
            }
            totalResourceCount += a_resources[i].Count;
        }
    }

    public Resource TakeResource(Resource.ResourceType a_type, int a_count)
    {
        if (m_resources.ContainsKey(a_type))
        {
            if (m_resources[a_type] > 0)
            {
                int take = Mathf.Min(a_count, m_resources[a_type]);
                m_resources[a_type] -= take;
                totalResourceCount -= take;
                Resource res = new Resource(a_type, take);
                return res;
            }
        }
        return new Resource(a_type, 0);
    }
    public Resource TakeResource(Resource.ResourceType a_type)
    {
        if (m_resources.ContainsKey(a_type))
        {
            if (m_resources[a_type] > 0)
            {
                Resource res = new Resource(a_type, m_resources[a_type]);
                totalResourceCount -= m_resources[a_type];
                m_resources[a_type] = 0;
                return res;
            }
        }
        return new Resource(a_type, 0);
    }
    public List<Resource> TakeAllResources()
    {
        List<Resource> resources = new List<Resource>();
        List<Resource.ResourceType> keys = m_resources.Keys.ToList();
        List<int> values = m_resources.Values.ToList();

        for (int i = 0; i < m_resources.Count; i++)
        {
            resources.Add(new Resource(keys[i], values[i]));
            m_resources[keys[i]] = 0;
        }
        totalResourceCount = 0;
        return resources;
    }

    public Resource PeekAtResource()
    {
        if (!HasResources)
            return null;

        List<Resource.ResourceType> keys = m_resources.Keys.ToList();
        List<int> values = m_resources.Values.ToList();

        for (int i = 0; i < m_resources.Count; i++)
        {
            if (values[i] != 0)
            {
                return new Resource(keys[i], values[i]);
            }
        }

        return null;
    }
    public Resource PeekAtResource(Resource.ResourceType a_type)
    {
        if (!m_resources.ContainsKey(a_type))
            return new Resource(a_type, 0);
        return new Resource(a_type, m_resources[a_type]);
    }


    public bool HasResources
    {
        get
        {
            return totalResourceCount > 0;
        }
    }
}