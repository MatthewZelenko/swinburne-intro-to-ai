﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BuildComponent : Component
{
    Dictionary<Resource.ResourceType, int> m_requiredResources;
    int m_requiredResourceCount;
    string m_builtSpriteSheet, m_builtSprite, m_unbuiltSprite;

    public BuildComponent(Entity a_entity, List<Resource> a_requiredResources, string a_spriteSheet, string a_unbuiltSprite, string a_builtSprite) : base(a_entity)
    {
        m_builtSpriteSheet = a_spriteSheet;
        m_builtSprite = a_builtSprite;
        m_unbuiltSprite = a_unbuiltSprite;
        RestartBuild(a_requiredResources);
    }

    public override void Init()
    {
    }
    public override void Update()
    {
    }
    public override void FixedUpdate()
    {
    }
    public override void Clear()
    {
    }


    public bool Build(Resource a_resource)
    {
        if (IsBuilt)
            return true;
        if (a_resource == null || a_resource.Count == 0 || (m_requiredResources.ContainsKey(a_resource.TypeOfResource) && m_requiredResources[a_resource.TypeOfResource] == 0))
            return false;

        int take = Mathf.Min(m_requiredResources[a_resource.TypeOfResource], a_resource.Count);
        //TODO:: return left resources
        m_requiredResources[a_resource.TypeOfResource] -= take;
        m_requiredResourceCount -= take;

        if (m_requiredResourceCount <= 0)
        {
            Complete();
            return true;
        }
        return false;
    }
    public void RestartBuild(List<Resource> a_requiredResources)
    {
        m_requiredResources = new Dictionary<Resource.ResourceType, int>();
        m_requiredResourceCount = 0;
        for (int i = 0; i < a_requiredResources.Count; i++)
        {
            m_requiredResources.Add(a_requiredResources[i].TypeOfResource, a_requiredResources[i].Count);
            m_requiredResourceCount += a_requiredResources[i].Count;
        }
        Parent.m_spriteRenderer.sprite = GameManager.GetInstance().m_spriteManager.GetSprite(m_builtSpriteSheet, m_unbuiltSprite);
    }
    public void Complete()
    {
        m_requiredResourceCount = 0;
        Parent.m_spriteRenderer.sprite = GameManager.GetInstance().m_spriteManager.GetSprite(m_builtSpriteSheet, m_builtSprite);
        m_requiredResources.Clear();
    }


    public bool IsBuilt { get { return m_requiredResourceCount <= 0; } }
}