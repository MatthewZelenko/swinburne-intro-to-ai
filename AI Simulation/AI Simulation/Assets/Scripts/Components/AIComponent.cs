﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIComponent : Component
{
    AIAction m_currentAction;
    Coroutine m_actionCoroutine;

    public AIComponent(Villager a_entity) : base(a_entity)
    {
        m_actionCoroutine = null;
    }
    public override void Init()
    {
    }
    public override void Clear()
    {
        StopAction();
    }
    public override void FixedUpdate()
    {
        if (m_currentAction != null && m_currentAction.IsFinished)
        {
            m_currentAction = null;
            m_actionCoroutine = null;
        }
    }

    public void StartAction(AIAction a_action)
    {
        m_currentAction = a_action;
        m_actionCoroutine = Parent.StartCoroutine(a_action.Run());
    }

    public void StopAction()
    {
        if (m_actionCoroutine != null)
        {
            Parent.StopCoroutine(m_actionCoroutine);
            m_actionCoroutine = null;
        }
        if (m_currentAction != null)
        {
            m_currentAction.Clear();
            m_currentAction = null;
        }
    }

    public bool RunningAction { get { return m_currentAction != null && m_actionCoroutine != null; } }
}