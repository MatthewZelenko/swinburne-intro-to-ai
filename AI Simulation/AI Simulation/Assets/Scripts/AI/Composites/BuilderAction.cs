﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BuilderAction : AIAction
{
    House m_targetHouse;
    Warehouse m_warehouse;

    public BuilderAction(Villager a_entity) : base(a_entity)
    {
    }

    public void Init()
    {
        HouseFactory mgr = GameManager.GetInstance().GetFactory<HouseFactory>();
        m_warehouse = mgr.GetWarehouse();
        m_targetHouse = mgr.GetBuildSite();
    }
    public override IEnumerator Run()
    {
        //TODO: Add new actions after each action.
        //1. Do you have wood?
        //2. No, Walk to warehouse
        //3. Get wood
        //4. Walk to House
        //5. Build House
        //6. Done

        Init();
        if (m_targetHouse == null)
        {
            Clear();
            yield break;
        }

        //Get resource from warehouse
        if (Parent.m_inventoryComponent.HasResources)
        {
            //Dispose of any non wood resources
            if (Parent.m_inventoryComponent.PeekAtResource(Resource.ResourceType.Wood).Count == 0)
            {
                yield return RunAction(new MoveToAction(m_warehouse, Parent));
                yield return RunAction(new GiveAllResourcesAction(m_warehouse.m_inventoryComponent, 2, Parent));
                yield return RunAction(new TakeResourceAction(m_warehouse.m_inventoryComponent, Resource.ResourceType.Wood, 1, 1, Parent));
            }
        }
        else
        {

            yield return RunAction(new MoveToAction(m_warehouse, Parent));
            yield return RunAction(new TakeResourceAction(m_warehouse.m_inventoryComponent, Resource.ResourceType.Wood, 1, 1, Parent));
        }
        if (Parent.m_inventoryComponent.PeekAtResource(Resource.ResourceType.Wood).Count == 0)
        {
            Clear();
            yield break;
        }

        //Go to house and build
        yield return RunAction(new MoveToAction(m_targetHouse, Parent));
        yield return RunAction(new BuildAction(m_targetHouse.m_buildComponent, Resource.ResourceType.Wood, 1, 5.0f, Parent));
        Parent.AddStat(Villager.StatTypes.JOBQUOTA, 25);

        Clear();
    }
    protected override void Clean()
    {
        m_targetHouse = null;
        m_warehouse = null;
    }
}