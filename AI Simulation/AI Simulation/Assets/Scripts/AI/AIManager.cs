﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIManager
{
    public List<AIComponent> m_aiComponents;
    List<FuzzyModule> m_fuzzyModules;
    public AIManager()
    {
        m_aiComponents = new List<AIComponent>();
    }
    public void Init()
    {
        m_fuzzyModules = new List<FuzzyModule>();
        m_fuzzyModules.Add(new FuzzyModule());
        m_fuzzyModules.Add(new FuzzyModule());

        {
            FuzzyVariable desirability = m_fuzzyModules[0].CreateFLV("Desirability");
            FuzzyTermSet unDesirable = desirability.AddLeftShoulderSet("UnDesirable", 0, 25, 50);
            FuzzyTermSet desirable = desirability.AddTriangleSet("Desirable", 25, 50, 75);
            FuzzyTermSet veryDesirable = desirability.AddRightShoulderSet("Very Desirable", 50, 75, 100);

            FuzzyVariable hunger = m_fuzzyModules[0].CreateFLV("Hunger");
            FuzzyTermSet starving = hunger.AddLeftShoulderSet("Starving", 0, 15, 40);
            FuzzyTermSet hungry = hunger.AddTriangleSet("Hungry", 15, 40, 65);
            FuzzyTermSet full = hunger.AddRightShoulderSet("Full", 40, 65, 100);

            m_fuzzyModules[0].AddRule(starving, unDesirable);
            m_fuzzyModules[0].AddRule(hungry, veryDesirable);
            m_fuzzyModules[0].AddRule(full, veryDesirable);
        }
        {
            FuzzyVariable desirability = m_fuzzyModules[1].CreateFLV("Desirability");
            FuzzyTermSet unDesirable = desirability.AddLeftShoulderSet("UnDesirable", 0, 25, 50);
            FuzzyTermSet desirable = desirability.AddTriangleSet("Desirable", 25, 50, 75);
            FuzzyTermSet veryDesirable = desirability.AddRightShoulderSet("Very Desirable", 50, 75, 100);

            FuzzyVariable jobQuota = m_fuzzyModules[1].CreateFLV("JobQuota");
            FuzzyTermSet unfulfilled = jobQuota.AddLeftShoulderSet("UnFulfilled", 0, 15, 40);
            FuzzyTermSet satisfied = jobQuota.AddTriangleSet("Satisfied", 15, 40, 65);
            FuzzyTermSet fulfilled = jobQuota.AddRightShoulderSet("Fulfilled", 40, 65, 100);

            m_fuzzyModules[1].AddRule(unfulfilled, unDesirable);
            m_fuzzyModules[1].AddRule(satisfied, desirable);
            m_fuzzyModules[1].AddRule(fulfilled, veryDesirable);
        }
    }
    public void FixedUpdate()
    {
        for (int i = m_aiComponents.Count - 1; i >= 0; i--)
        {
            if (m_aiComponents[i] == null || m_aiComponents[i].Parent == null || m_aiComponents[i].Parent.Dead)
            {
                if (m_aiComponents[i] != null)
                {
                    m_aiComponents[i].StopAction();
                }
                m_aiComponents.RemoveAt(i);
            }
            else
            {
                m_aiComponents[i].FixedUpdate();
            }
        }
        for (int i = 0; i < m_aiComponents.Count; i++)
        {
            if (m_aiComponents[i].RunningAction)
                continue;
            Villager villager = m_aiComponents[i].Parent as Villager;

            m_fuzzyModules[0].Fuzzify("Hunger", villager.GetStat(Villager.StatTypes.HUNGER));
            double hunger = m_fuzzyModules[0].DeFuzzify("Desirability", FuzzyModule.DefuzzifyType.MAX_AV);

            m_fuzzyModules[1].Fuzzify("JobQuota", villager.GetStat(Villager.StatTypes.JOBQUOTA));
            double jobQuota = m_fuzzyModules[1].DeFuzzify("Desirability", FuzzyModule.DefuzzifyType.MAX_AV);

            if (hunger < jobQuota)
            {
                //Debug.Log("Eat");
                if (GameManager.GetInstance().GetFactory<HouseFactory>().GetWarehouse().m_inventoryComponent.PeekAtResource(Resource.ResourceType.Food).Count != 0)
                {
                    m_aiComponents[i].StartAction(new EatAction(villager));
                }
                else
                {
                    //Debug.Log("Wander");
                    m_aiComponents[i].StartAction(new WanderMode(2, 5, 1, 6, villager));
                }
            }
            else
            {
                //Debug.Log("Job");
                switch (villager.Job)
                {
                    case Villager.JobType.Lumberjack:
                        m_aiComponents[i].StartAction(new LumberjackAction(villager));
                        break;
                    case Villager.JobType.Builder:
                        m_aiComponents[i].StartAction(new BuilderAction(villager));
                        break;
                    case Villager.JobType.Farmer:
                        m_aiComponents[i].StartAction(new FarmingAction(villager));
                        break;
                    default:
                        m_aiComponents[i].StartAction(new WanderMode(2, 5, 1, 6, villager));
                        break;
                }
            }
        }
    }
    public AIComponent CreateComponent(Villager a_parent)
    {
        m_aiComponents.Add(new AIComponent(a_parent));
        return m_aiComponents[m_aiComponents.Count - 1];
    }

}