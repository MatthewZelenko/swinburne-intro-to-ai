﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class FuzzyTerm
{
    public abstract double GetDOM();
    public abstract void ClearDOM();
    public abstract void ORWithDOM(double a_value);
}