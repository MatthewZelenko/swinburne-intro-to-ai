﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FuzzyModule
{
    public enum DefuzzifyType
    {
        MAX_AV,
        CENTROID
    }
    public const int NUM_CENTROID_SAMPLES = 15;

    Dictionary<string, FuzzyVariable> m_fuzzyVariables;
    List<FuzzyRule> m_rules;


    public FuzzyModule()
    {
        m_fuzzyVariables = new Dictionary<string, FuzzyVariable>();
        m_rules = new List<FuzzyRule>();
    }
    ~FuzzyModule()
    {

    }


    public FuzzyVariable CreateFLV(string a_variableName)
    {
        return m_fuzzyVariables[a_variableName] = new FuzzyVariable();        
    }
    public void AddRule(FuzzyTerm a_antecedent, FuzzyTerm a_consequence)
    {
        m_rules.Add(new FuzzyRule(a_antecedent, a_consequence));
    }
    public void Fuzzify(string a_nameOfFLV, double a_value)
    {
        m_fuzzyVariables[a_nameOfFLV].Fuzzify(a_value);
    }
    public double DeFuzzify(string a_key, DefuzzifyType a_method)
    {
        SetConfidencesOfConsequentsToZero();

        foreach (FuzzyRule r in m_rules)
        {
            r.Calculate();
        }

        switch (a_method)
        {
            case DefuzzifyType.MAX_AV:
                return m_fuzzyVariables[a_key].DeFuzzifyMaxAv();
            case DefuzzifyType.CENTROID:
                return m_fuzzyVariables[a_key].DeFuzzifyCentroid(NUM_CENTROID_SAMPLES);
            default:
                return 0.0;
        }
    }


    public void SetConfidencesOfConsequentsToZero()
    {
        foreach (FuzzyRule r in m_rules)
        {
            r.SetConfidenceOfConsequentToZero();
        }
    }
}