﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FuzzyTermAnd : FuzzyTerm
{
    List<FuzzyTerm> m_terms;

    public FuzzyTermAnd(FuzzyTerm a_op1, FuzzyTerm a_op2)
    {
        m_terms = new List<FuzzyTerm>();
        m_terms.Add(a_op1);
        m_terms.Add(a_op2);
    }
    public FuzzyTermAnd(FuzzyTerm a_op1, FuzzyTerm a_op2, FuzzyTerm a_op3)
    {
        m_terms = new List<FuzzyTerm>();
        m_terms.Add(a_op1);
        m_terms.Add(a_op2);
        m_terms.Add(a_op3);
    }
    public FuzzyTermAnd(FuzzyTerm a_op1, FuzzyTerm a_op2, FuzzyTerm a_op3, FuzzyTerm a_op4)
    {
        m_terms = new List<FuzzyTerm>();
        m_terms.Add(a_op1);
        m_terms.Add(a_op2);
        m_terms.Add(a_op3);
        m_terms.Add(a_op4);
    }

    public override void ClearDOM()
    {
        foreach (FuzzyTerm t in m_terms)
        {
            t.ClearDOM();
        }
    }

    public override double GetDOM()
    {
        double smallest = double.MaxValue;

        foreach (FuzzyTerm t in m_terms)
        {
            if (t.GetDOM() < smallest)
            {
                smallest = t.GetDOM();
            }
        }
        return smallest;
    }

    public override void ORWithDOM(double a_value)
    {
        foreach (FuzzyTerm t in m_terms)
        {
            t.ORWithDOM(a_value);
        }
    }
}