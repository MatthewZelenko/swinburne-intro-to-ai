﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class FuzzySet
{
    public FuzzySet(double a_repValue)
    {
        RepresentativeValue = a_repValue;
        DOM = 0.0;
    }

    public abstract double CalculateDOM(double a_value);
    public void ORwithDOM(double a_value) { if (a_value > DOM) DOM = a_value; }
    public void ClearDOM() { DOM = 0.0; }

    public double DOM { get; set; }
    public double RepresentativeValue { get; private set; }
}