﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FuzzyRule
{
    FuzzyTerm m_antecedant, m_consequence;

    public FuzzyRule(FuzzyTerm a_antecedant, FuzzyTerm a_consequence)
    {
        m_antecedant = a_antecedant;
        m_consequence = a_consequence;
    }

    public void SetConfidenceOfConsequentToZero() { m_consequence.ClearDOM(); }
    public void Calculate()
    {
        m_consequence.ORWithDOM(m_antecedant.GetDOM());
    }
}