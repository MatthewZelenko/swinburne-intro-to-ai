﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Assertions;

public class FuzzyVariable
{
    Dictionary<string, FuzzySet> m_memberSets;
    double m_minRange, m_maxRange;


    public FuzzyVariable()
    {
        m_memberSets = new Dictionary<string, FuzzySet>();
        m_minRange = m_maxRange = 0.0;
    }
    void AdjustRangeToFit(double a_min, double a_max)
    {
        if (a_min < m_minRange) m_minRange = a_min;
        if (a_max > m_maxRange) m_maxRange = a_max;
    }
    public FuzzyTermSet AddLeftShoulderSet(string a_name, double a_minBound, double a_peak, double a_maxBound)
    {
        m_memberSets[a_name] = new FuzzySetLeftShoulder(a_peak, a_peak - a_minBound, a_maxBound - a_peak);
        AdjustRangeToFit(a_minBound, a_maxBound);
        return new FuzzyTermSet(m_memberSets[a_name]);
    }
    public FuzzyTermSet AddRightShoulderSet(string a_name, double a_minBound, double a_peak, double a_maxBound)
    {
        m_memberSets[a_name] = new FuzzySetRightShoulder(a_peak, a_peak - a_minBound, a_maxBound - a_peak);
        AdjustRangeToFit(a_minBound, a_maxBound);
        return new FuzzyTermSet(m_memberSets[a_name]);
    }
    public FuzzyTermSet AddTriangleSet(string a_name, double a_minBound, double a_peak, double a_maxBound)
    {
        m_memberSets[a_name] = new FuzzySetTriangle(a_peak, a_peak - a_minBound, a_maxBound - a_peak);
        AdjustRangeToFit(a_minBound, a_maxBound);
        return new FuzzyTermSet(m_memberSets[a_name]);
    }
    public void Fuzzify(double a_value)
    {
        Assert.IsTrue((a_value >= m_minRange && a_value <= m_maxRange), "FuzzyVariable: Value out of range.");

        Dictionary<string, FuzzySet>.Enumerator enumerator = m_memberSets.GetEnumerator();

        while (enumerator.MoveNext())
        {
            enumerator.Current.Value.DOM = enumerator.Current.Value.CalculateDOM(a_value);
        }
    }
    public double DeFuzzifyMaxAv()
    {
        double bottom = 0.0;
        double top = 0.0;

        Dictionary<string, FuzzySet>.Enumerator enumerator = m_memberSets.GetEnumerator();

        while (enumerator.MoveNext())
        {
            bottom += enumerator.Current.Value.DOM;
            top += enumerator.Current.Value.RepresentativeValue * enumerator.Current.Value.DOM;
        }
        if (bottom == 0) return 0.0;
        return top / bottom;
    }
    public double DeFuzzifyCentroid(int a_numSamples)
    {
        double stepSize = (m_maxRange - m_minRange) / (double)a_numSamples;
        double totalArea = 0.0;
        double sumOfMoments = 0.0;

        for (int i = 1; i < a_numSamples; i++)
        {
            Dictionary<string, FuzzySet>.Enumerator enumerator = m_memberSets.GetEnumerator();
            while (enumerator.MoveNext())
            {
                double contribution = Math.Min(enumerator.Current.Value.CalculateDOM(m_minRange + (i * stepSize)), enumerator.Current.Value.DOM);
                totalArea += contribution;
                sumOfMoments += (m_minRange + (i * stepSize)) * contribution;
            }
        }
        if (totalArea == 0.0f) return 0.0;
        return sumOfMoments / totalArea;
    }
}