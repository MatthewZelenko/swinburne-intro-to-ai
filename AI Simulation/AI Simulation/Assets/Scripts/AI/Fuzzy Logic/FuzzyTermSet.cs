﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FuzzyTermSet : FuzzyTerm
{
    FuzzySet m_set;
    public FuzzyTermSet(FuzzySet a_set)
    {
        m_set = a_set;
    }

    public override void ClearDOM()
    {
        m_set.ClearDOM();
    }

    public override double GetDOM()
    {
        return m_set.DOM;
    }

    public override void ORWithDOM(double a_value)
    {
        m_set.ORwithDOM(a_value);
    }
}