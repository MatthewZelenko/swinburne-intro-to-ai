﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GiveAllResourcesAction : AIAction
{
    InventoryComponent m_inventoryComponent;
    float m_waitTime;
    float m_elapse;

    public GiveAllResourcesAction(InventoryComponent a_inventoryComponent, float a_waitTime, Villager a_entity) : base(a_entity)
    {
        m_inventoryComponent = a_inventoryComponent;
        m_waitTime = a_waitTime;
    }

    public override IEnumerator Run()
    {
        while (m_elapse < m_waitTime)
        {
            m_elapse += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        m_inventoryComponent.AddAllResources(Parent.m_inventoryComponent.TakeAllResources());
        Parent.SetSprite("Villager_Idle");
        Parent.SetMovementSpeed();
    }

    protected override void Clean()
    {
    }
}