﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BuildAction : AIAction
{
    float m_waitTime;
    float m_elapse;
    BuildComponent m_target;
    Resource.ResourceType m_type;
    int m_resourceCount;

    public BuildAction(BuildComponent a_target, Resource.ResourceType a_type, int a_count, float a_waitTime, Villager a_entity) : base(a_entity)
    {
        m_waitTime = a_waitTime;
        m_target = a_target;
        m_resourceCount = a_count;
        m_type = a_type;
    }

    public override IEnumerator Run()
    {
        while (m_elapse < m_waitTime)
        {
            m_elapse += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        Resource res = Parent.m_inventoryComponent.TakeResource(m_type, m_resourceCount);
        m_target.Build(res);
        Parent.SetSprite("Villager_Idle");
        Parent.SetMovementSpeed();
    }
    protected override void Clean()
    {
    }
}