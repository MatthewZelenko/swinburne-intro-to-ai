﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MoveToAction : AIAction
{
    public Entity m_target;
    public float m_cancelTime, m_cancelElapse;

    public MoveToAction(Entity a_target, Villager a_entity, int a_cancelTime = 1000) : base(a_entity)
    {
        m_target = a_target;
        m_cancelTime = a_cancelTime;
    }

    protected override void Clean()
    {
    }

    public void Init()
    {
        if (!Parent.m_inventoryComponent.HasResources)
        {
            /*
            Resource res = Parent.m_inventoryComponent.PeekAtResource();
            if (res != null)
            {
                Parent.SetSprite("Villager_Hold_" + res.TypeOfResource.ToString());
                return;
            }
            */
            Parent.SetSprite("Villager_Idle");
        }
    }

    public override IEnumerator Run()
    {
        Init();

        if (m_target == null)
            yield break;

        float speed = Parent.m_movementSpeed * Time.fixedDeltaTime;

        float diff = m_target.transform.position.x - Parent.transform.position.x;
        float movement = (diff < 0 ? -1 : 1) * speed;
        if (movement < 0)
        {
            Parent.m_spriteRenderer.flipX = true;
        }

        while (m_cancelElapse < m_cancelTime)
        {
            m_cancelElapse += Time.fixedDeltaTime;
            diff = m_target.transform.position.x - Parent.transform.position.x;
            if (Mathf.Abs(diff) > speed + 0.01f)
            {
                Parent.transform.Translate(movement, 0, 0);
            }
            else
            {
                Parent.transform.position = new Vector3(m_target.transform.position.x, Parent.transform.position.y, Parent.transform.position.z);
                Parent.m_spriteRenderer.flipX = false;
                yield break;
            }
            yield return new WaitForFixedUpdate();
        }
        Parent.m_spriteRenderer.flipX = false;
    }
}