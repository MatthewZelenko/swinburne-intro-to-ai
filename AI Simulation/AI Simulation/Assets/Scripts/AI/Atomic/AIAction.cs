﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIAction
{
    public enum Mode
    {
        Wander,
        Job
    }

    public AIAction(Villager a_entity)
    {
        Parent = a_entity;
        IsFinished = false;
    }

    public Coroutine RunAction(AIAction a_action)
    {
        CurrentAction = a_action;
        return Parent.StartCoroutine(a_action.Run());
    }
    /// <summary>
    /// Returns -1 if not finished, 0 if added new modes on top, 1 if completely finished
    /// </summary>
    public abstract IEnumerator Run();
    public void Clear()
    {
        if (CurrentAction != null)
        {
            CurrentAction.Clear();
            CurrentAction = null;
        }
        Clean();
        IsFinished = true;
    }
    protected abstract void Clean();

    public Villager Parent { get; private set; }
    public bool IsFinished { get; private set; }
    AIAction CurrentAction { get; set; }
}