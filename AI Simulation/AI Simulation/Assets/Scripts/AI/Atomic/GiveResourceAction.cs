﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GiveResourceAction : AIAction
{
    InventoryComponent m_inventoryComponent;
    int m_count;
    Resource.ResourceType m_type;
    float m_waitTime;
    float m_elapse;

    public GiveResourceAction(InventoryComponent a_inventoryComponent, Resource.ResourceType a_type, int a_count, float a_waitTime, Villager a_entity) : base(a_entity)
    {
        m_inventoryComponent = a_inventoryComponent;
        m_waitTime = a_waitTime;
        m_count = a_count;
        m_type = a_type;
    }

    public override IEnumerator Run()
    {
        while (m_elapse < m_waitTime)
        {
            m_elapse += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        m_inventoryComponent.AddResource(Parent.m_inventoryComponent.TakeResource(m_type, m_count));
        Parent.SetSprite("Villager_Idle");
        Parent.SetMovementSpeed();
    }
    protected override void Clean()
    {
    }
}