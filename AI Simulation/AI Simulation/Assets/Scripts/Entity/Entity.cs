﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Entity : MonoBehaviour
{
    public int m_factoryID;
    List<Component> m_components;
    public SpriteRenderer m_spriteRenderer;

    public Entity()
    {
        m_components = new List<Component>();
        Dead = false;
    }
    public virtual void Init()
    {

    }
    public void Clear()
    {
        for (int i = 0; i < m_components.Count; i++)
        {
            m_components[i].Clear();
        }
        m_components.Clear();
        Clean();
    }
    protected virtual void Clean()
    {

    }

    public float MagnitudeTo(Entity a_entity)
    {
        if (a_entity == null)
            return -1;

        return Mathf.Abs(a_entity.transform.position.x - transform.position.x);
    }
    public T AttachComponent<T>(T a_component) where T : Component
    {
        m_components.Add(a_component);
        return a_component;
    }
    public T RetrieveComponent<T>() where T : Component
    {
        for (int i = 0; i < m_components.Count; i++)
        {
            if (m_components[i].GetType() == typeof(T))
            {
                return m_components[i] as T;
            }
        }
        return default(T);
    }

    public bool Dead
    {
        get; set;
    }
}