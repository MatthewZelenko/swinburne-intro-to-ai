﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Villager : Entity
{
    public enum JobType
    {
        Lumberjack,
        Builder,
        Farmer
    }

    public AIComponent m_aiComponent;
    public InventoryComponent m_inventoryComponent;

    public const float NORMAL_MOVEMENT_SPEED = 30;

    public static float[] STAT_DECREASES = { 0.1f, 0.5f };

    public enum StatTypes
    {
        HUNGER,
        //THIRST,
        //ENERGY,
        JOBQUOTA,
        //LUST,
        SIZE
    }


    public List<float> m_stats;

    public void SetMovementSpeed(float a_speed = NORMAL_MOVEMENT_SPEED)
    {
        m_movementSpeed = a_speed;
    }

    public float m_movementSpeed;

    void Awake()
    {
    }
    private void FixedUpdate()
    {
        if (Dead)
            return;

        for (int i = 0; i < (int)StatTypes.SIZE; i++)
        {
            AddStat((StatTypes)i, -STAT_DECREASES[i] * Time.fixedDeltaTime);
            if (i == (int)StatTypes.HUNGER)
            {
                if (m_stats[i] <= 0.0f)
                {
                    Dead = true;
                }
            }
        }
        //Old school agent decision
        //if (!m_aiComponent.RunningAction)
        //{
        //    //Calculate new action
        //    if (Job == JobType.Lumberjack)
        //    {
        //        m_aiComponent.StartAction(new LumberjackAction(this));
        //    }
        //    else if (Job == JobType.Builder)
        //    {
        //        m_aiComponent.StartAction(new BuilderAction(this));
        //    }
        //    else if (Job == JobType.Farmer)
        //    {
        //        m_aiComponent.StartAction(new FarmingAction(this));
        //    }
        //}
    }
    void Update()
    {

    }

    public override void Init()
    {
        m_movementSpeed = NORMAL_MOVEMENT_SPEED;
        m_stats = new List<float>();
        for (int i = 0; i < (int)StatTypes.SIZE; i++)
        {
            m_stats.Add(100);
        }

        m_aiComponent = AttachComponent(GameManager.GetInstance().m_aiManager.CreateComponent(this));
        m_aiComponent.Init();
        m_inventoryComponent = AttachComponent(new InventoryComponent(this));
        m_inventoryComponent.Init();
    }

    public void SetSprite(string a_spriteName)
    {
        Sprite sprite = GameManager.GetInstance().m_spriteManager.GetSprite("Villager", a_spriteName);
        if (sprite)
        {
            m_spriteRenderer.sprite = sprite;
        }
    }
    public void AddStat(StatTypes a_statType, float a_count)
    {
        m_stats[(int)a_statType] = Mathf.Clamp(m_stats[(int)a_statType] + a_count, 0, 100);
    }
    public float GetStat(StatTypes a_statType)
    {
        return m_stats[(int)a_statType];
    }

    public JobType Job { get; set; }
}