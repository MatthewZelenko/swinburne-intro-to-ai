﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FarmFactory : Factory
{
    public Farm m_farmPrefab;

    public FarmFactory()
    {
    }

    public Farm CreateFarm()
    {
        Farm farm = base.CreateEntity(m_farmPrefab) as Farm;
        farm.Init();
        return farm;
    }

    public Farm GetAvailableFarm()
    {
        Farm farm = null;
        for (int i = 0; i < m_entities.Count; i++)
        {
            farm = m_entities[i] as Farm;
            if (farm && (!farm.m_residentComponent.Full))
                return farm;
        }
        farm = CreateFarm();
        LevelManager mgr = GameManager.GetInstance().m_levelManager;
        mgr.m_layers[(int)LevelManager.LevelLayerTypes.BUILDING].AddEntity(farm);
        mgr.AddLeftEntity(farm);
        return farm;
    }
}