﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseFactory : Factory
{
    public House m_housePrefab;
    public Warehouse m_warehousePrefab;

    public Warehouse CreateWarehouse()
    {
        Warehouse warehouse = base.CreateEntity(m_warehousePrefab) as Warehouse;
        warehouse.Init();
        return warehouse;
    }
    public House CreateHouse(bool a_complete = false)
    {
        House house = base.CreateEntity(m_housePrefab) as House;
        house.Init();
        if (a_complete)
            house.m_buildComponent.Complete();

        return house;
    }

    public Warehouse GetWarehouse()
    {
        for (int i = 0; i < m_entities.Count; i++)
        {
            Warehouse warehouse = m_entities[i] as Warehouse;
            if (warehouse)
            {
                return warehouse;
            }
        }
        return null;
    }
    public House GetBuildSite()
    {
        House house = null;
        for (int i = m_entities.Count - 1; i >= 0; i--)
        {
            house = m_entities[i] as House;
            if (house)
            {
                if (house.m_buildComponent == null || !house.m_buildComponent.IsBuilt)
                {
                    return house;
                }
            }
        }
        house = CreateHouse();
        LevelManager mgr = GameManager.GetInstance().m_levelManager;
        mgr.m_layers[(int)LevelManager.LevelLayerTypes.BUILDING].AddEntity(house);
        mgr.AddLeftEntity(house);
        return house;
    }
}