﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class VillagerFactory : Factory
{
    public Villager m_villagerPrefab;

    void Awake()
    {
    }
    void Update()
    {

    }

    private void FixedUpdate()
    {
        CheckDeadEntities();
    }

    public Villager CreateVillager(Villager.JobType a_type)
    {
        Villager v = base.CreateEntity(m_villagerPrefab) as Villager;
        v.Init();
        v.Job = a_type;
        return v;
    }
}