﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestFactory : Factory
{
    public Tree m_treePrefab;

    void Awake()
    {
    }
    void Update()
    {

    }

    private void FixedUpdate()
    {
        CheckDeadEntities();
    }

    public Tree CreateTree()
    {
        Tree tree = base.CreateEntity(m_treePrefab) as Tree;
        tree.Init();
        return tree;
    }

    public Tree GetUnoccupiedTree()
    {
        if (m_entities.Count == 0)
            return null;

        for (int i = 0; i < m_entities.Count; i++)
        {
            Tree tree = m_entities[i] as Tree;
            if (tree)
            {
                if (!tree.m_inventoryComponent.HasResources)
                {
                    tree.Dead = true;
                    continue;
                }
                if (!tree.m_residentComponent.Full)
                    return tree;
            }
        }
        return null;
    }
}