﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Resource
{
    public enum ResourceType
    {
        Wood,
        Food,
        Water
    }
    
    public Resource(ResourceType a_type, int a_count)
    {
        TypeOfResource = a_type;
        Count = a_count;
    }

    public ResourceType TypeOfResource { get; private set; }
    public int Count { get; private set; }
}