﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : StaticEntity
{
    public InventoryComponent m_inventoryComponent;
    public ResidentComponent m_residentComponent;

    public Tree() : base(1)
    {
    }

    public override void Init()
    {
        m_inventoryComponent = AttachComponent(new InventoryComponent(this));
        m_inventoryComponent.AddResource(new Resource(Resource.ResourceType.Wood, 10));
        m_residentComponent = AttachComponent(new ResidentComponent(this, 1));
    }
    void Update()
    {

    }
    public Resource Chop()
    {
        if (Dead)
        {
            return new Resource(Resource.ResourceType.Wood, 0);
        }

        Resource res = m_inventoryComponent.TakeResource(Resource.ResourceType.Wood, 1);

        if (!m_inventoryComponent.HasResources)
            Dead = true;
        return res;
    }
}