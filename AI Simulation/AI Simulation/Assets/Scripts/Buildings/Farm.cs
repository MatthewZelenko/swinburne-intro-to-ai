﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Farm : StaticEntity
{
    public BuildComponent m_buildComponent;
    public InventoryComponent m_inventoryComponent;
    public ResidentComponent m_residentComponent;
    public Farm() : base(3)
    {
    }

    public override void Init()
    {
        m_buildComponent = AttachComponent(new BuildComponent(this, new List<Resource>() { new Resource(Resource.ResourceType.Wood, 4) }, "Farm", "Farm_Unbuilt", "Farm_Built"));
        m_inventoryComponent = AttachComponent(new InventoryComponent(this));
        m_inventoryComponent.AddResource(new Resource(Resource.ResourceType.Food, 10));
        m_residentComponent = AttachComponent(new ResidentComponent(this, 2));
    }

    public Resource Hoe()
    {
        if (Dead || !m_buildComponent.IsBuilt)
        {
            return new Resource(Resource.ResourceType.Food, 0);
        }

        Resource res = m_inventoryComponent.TakeResource(Resource.ResourceType.Food, 1);

        if (!m_inventoryComponent.HasResources)
            m_buildComponent.RestartBuild(new List<Resource>() { new Resource(Resource.ResourceType.Food, 2) });
        return res;
    }
    //TODO: Have farm regenerate crops instead.
    protected override void Clean()
    {
    }
}