﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class House : StaticEntity
{
    public const int MAX_NUM_OF_RESIDENTS = 2;

    public BuildComponent m_buildComponent;
    public ResidentComponent m_residentComponent;
    public House() : base(4)
    {

    }

    public override void Init()
    {
        m_buildComponent = AttachComponent(new BuildComponent(this, new List<Resource>() { new Resource(Resource.ResourceType.Wood, 5) }, "House", "House_Unbuilt", "House_Built"));
        m_residentComponent = AttachComponent(new ResidentComponent(this, MAX_NUM_OF_RESIDENTS));
    }

    protected override void Clean()
    {
    }
}