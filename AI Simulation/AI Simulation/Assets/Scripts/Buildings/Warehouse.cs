﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warehouse : StaticEntity
{
    public InventoryComponent m_inventoryComponent;
    public float elapse = 0.0f;

    public Warehouse() : base(4)
    {
    }

    public override void Init()
    {
        m_inventoryComponent = AttachComponent(new InventoryComponent(this));
    }
    private void FixedUpdate()
    {
        elapse += Time.fixedDeltaTime;
        if (elapse >= 5.0f)
        {
            elapse = 0.0f;
            m_inventoryComponent.WriteToConsole();
        }
    }
    void Update()
    {

    }
}