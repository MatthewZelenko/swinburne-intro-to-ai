#pragma once
#include <iostream>
#include <vector>
#include "Action.h"
#include "Goal.h"

class WorldModel
{
public:
	WorldModel()
	{
		m_currentAction = 0;
	}
	~WorldModel()
	{

	}

	int CalulateDiscontentment()
	{
		int discontentment = 0;

		for (int i = 0; i < m_actions.size(); i++)
		{
			discontentment += m_actions[i].CalulateDiscontentment(m_goals);
		}
		return discontentment;
	}

	Action* NextAction()
	{
		if (m_currentAction >= m_actions.size())
			return nullptr;
		return &m_actions[m_currentAction++];
	}

	void ApplyAction(Action* a_action)
	{
		for (int i = 0; i < m_goals.size(); i++)
		{
			m_goals[i].m_value += a_action->GetGoalChange(m_goals[i]);
		}
		m_currentAction = 0;
	}

	void AddAction(Action a_action)
	{
		m_actions.push_back(a_action);
	}

	void AddGoal(Goal a_goal)
	{
		m_goals.push_back(a_goal);
	}

	void PrintGoals()
	{
		std::cout << "Goals:" << std::endl;
		for (int i = 0; i < m_goals.size(); i++)
		{
			std::cout << "\t" << m_goals[i].m_name << ": " << m_goals[i].m_value << std::endl;
		}
	}
	void PrintDetails()
	{
		PrintGoals();


		std::cout << std::endl << "Actions: " << std::endl;
		for (int i = 0; i < m_actions.size(); i++)
		{
			std::cout << "\t" << m_actions[i].String() << std::endl;
		}
	}

	bool IsDone()
	{
		for (int i = 0; i < m_goals.size(); i++)
		{
			if (m_goals[i].m_value <= 0)
			{
				return true;
			}
		}
		return false;
	}

private:
	int m_currentAction;
	std::vector<Action> m_actions;
	std::vector<Goal> m_goals;
};