#pragma once
#include "Goal.h"
#include <map>
#include <vector>

struct Action
{
	Action()
	{
	}
	Action(std::string a_name)
	{
		m_name = a_name;
	}
	int GetGoalChange(Goal a_goal)
	{
		if (m_sideEffects.find(a_goal.m_name) == m_sideEffects.end())
			return 0;
		return m_sideEffects[a_goal.m_name];
	}

	int CalulateDiscontentment(std::vector<Goal> a_goals)
	{
		int discontentment = 0;

		for (int i = 0; i < a_goals.size(); i++)
		{
			int newValue = a_goals[i].m_value + GetGoalChange(a_goals[i]);

			discontentment += Goal::GetDiscontentment(newValue);
		}
		return discontentment;
	}

	std::string String()
	{
		std::map<std::string, int>::iterator it;

		std::string ret = m_name + "( ";
		for (it = m_sideEffects.begin(); it != m_sideEffects.end(); it++)
		{
			ret += (*it).first + ": " + std::to_string((*it).second) + " ";
		}
		ret += ")";
		return ret;
	}


	std::string m_name = "Action";
	//GOAL[effect]
	std::map<std::string, int> m_sideEffects;
};