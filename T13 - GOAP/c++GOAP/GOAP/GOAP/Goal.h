#pragma once
#include <string>

struct Goal
{
	Goal(std::string a_name, int a_value)
	{
		m_name = a_name;
		m_value = a_value;
	}
	static int GetDiscontentment(int a_newValue)
	{
		return a_newValue * a_newValue;
	}

	std::string m_name;
	int m_value;
};