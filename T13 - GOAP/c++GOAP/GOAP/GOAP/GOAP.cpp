#include "WorldModel.h"
#include "GOAP.h"

Action PlanAction(WorldModel a_model, int a_maxDepth = 3)
{
	WorldModel* models = new WorldModel[a_maxDepth + 1];
	Action* actions = new Action[a_maxDepth + 1];

	models[0] = a_model;
	int currentDepth = 0;

	Action bestAction = Action();
	int bestValue = INT32_MAX;

	std::cout << "\n\nPlans:" << std::endl;

	while (currentDepth >= 0)
	{
		int currentValue = models[currentDepth].CalulateDiscontentment();
		if (currentDepth >= a_maxDepth)
		{
			if (currentValue < bestValue)
			{
				bestValue = currentValue;
				bestAction = actions[0];
			}
			currentDepth--;
			continue;
		}
		Action* nextAction = models[currentDepth].NextAction();
		if (nextAction != nullptr)
		{
			models[currentDepth + 1] = models[currentDepth];

			actions[currentDepth] = *nextAction;
			models[currentDepth + 1].ApplyAction(nextAction);

			currentDepth++;

			std::string push = "";
			for (int i = 1; i < currentDepth; i++)
			{
				push += "\t";
			}
			push += "--";

			std::cout << push << nextAction->String() << "____" << currentValue << std::endl;
		}
		else
		{
			currentDepth--;
		}
	}

	delete[] actions;
	delete[] models;
	return bestAction;
}


int main()
{
	WorldModel baseModel;
	baseModel.AddGoal(Goal("Eat", 4));
	baseModel.AddGoal(Goal("Sleep", 3));


	Action action = Action("Get raw food");
	action.m_sideEffects["Eat"] = -3;
	action.m_sideEffects["Sleep"] = 1;
	baseModel.AddAction(action);

	action = Action("Sleep in bed");
	action.m_sideEffects["Eat"] = 3;
	action.m_sideEffects["Sleep"] = -4;
	baseModel.AddAction(action);


	while (!baseModel.IsDone())
	{
		baseModel.PrintDetails();
		Action chosenAction = PlanAction(baseModel);
		if (chosenAction.m_name != "Action")
		{
			std::cout << "\nChosen action: " << chosenAction.String() << std::endl;
		}
		baseModel.ApplyAction(&chosenAction);
		baseModel.PrintGoals();
		system("pause");
		std::cout << "\n\n\n";
	}

	return 0;
}