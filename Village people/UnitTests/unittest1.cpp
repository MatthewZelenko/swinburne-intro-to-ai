#include "CppUnitTest.h"
#include <Input.h>
#include <GLFW\glfw3.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(InputTest)
	{
	public:
		
		TEST_METHOD(TestIsKey)
		{
			Input& input = Input::Get();
			
			Assert::IsTrue(input.IsKey(KEYCODE_ZERO, KEYSTATE_UP));
		}
	};
}