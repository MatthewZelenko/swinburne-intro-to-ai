#pragma once
#include "Texture.h"
#include "Shader.h"
#include <GLM\glm.hpp>

class Renderer
{
public:
	Renderer();
	~Renderer();

	void Load(float a_left, float a_right, float a_top, float a_bottom);
	void Unload();

	//Uses the passed in shader or the default shader. Creates an MVP and sends it to "MVP" on the shader.
	void Render(Shader* a_shader, Texture& a_texture, const glm::vec2& a_position, const glm::vec2& a_size, float a_rotation);
	//DrawVertices of a square
	void DrawSquareVerts();

private:
	unsigned int m_vao, m_vbo, m_ebo;
	glm::mat4 m_projectionMatrix;
	glm::mat4 m_viewMatrix;

	Shader m_defaultShader;
};