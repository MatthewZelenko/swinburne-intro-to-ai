#include "Game.h"
#include "Input.h"

Game::Game(std::string a_title, int a_gameWidth, int a_gameHeight) : Application(a_title),
m_gameWidth(a_gameWidth), m_gameHeight(a_gameHeight)
{
}
Game::~Game()
{
}
void Game::Load()
{
	m_renderer.Load(0, m_gameWidth, 0, m_gameHeight);
	m_texture.CreateFromFile("Resources/Gear.png");
}
void Game::Unload()
{
	m_texture.Destroy();
	m_renderer.Unload();
}

void Game::ProcessInput(float a_deltaTime)
{
	if (Input::Get().IsKey(KEYCODE_ESCAPE, KEYSTATE_PRESSED | KEYSTATE_DOWN))
		Quit();
}
void Game::FixedUpdate(float a_deltaTime)
{
}
void Game::Update(float a_deltaTime)
{
}
void Game::Render(float a_deltaTime)
{
	m_window.Clear();
	m_renderer.Render(nullptr, m_texture, glm::vec2(960.0f, 540.0f), glm::vec2(100.0f, 100.0f), 0.0f);
	m_window.SwapBuffers();
}