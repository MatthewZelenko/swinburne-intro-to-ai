#include "Texture.h"
#include <GLEW\glew.h>
#include <LodePNG\lodepng.h>
#include <iostream>
using namespace std;
Texture::Texture()
{
	m_id = 0;
	m_wrapSParam = GL_REPEAT;
	m_wrapTParam = GL_REPEAT;
	m_minFilterParam = GL_LINEAR;
	m_magFilterParam = GL_LINEAR;
}
Texture::Texture(const Texture& other)
{
	m_id = other.m_id;
	m_strName = other.m_strName;
	m_width = other.m_width;
	m_height = other.m_height;
	m_wrapSParam = other.m_wrapSParam;
	m_wrapTParam = other.m_wrapTParam;
	m_minFilterParam = other.m_minFilterParam;
	m_magFilterParam = other.m_magFilterParam;
}
Texture::~Texture()
{
}
void Texture::Destroy()
{
	if (m_id > 0)
	{
		glDeleteTextures(1, &m_id);
		m_id = 0;
	}
}

Texture& Texture::operator=(const Texture& other)
{
	m_id = other.m_id;
	m_strName = other.m_strName;
	m_width = other.m_width;
	m_height = other.m_height;
	m_wrapSParam = other.m_wrapSParam;
	m_wrapTParam = other.m_wrapTParam;
	m_minFilterParam = other.m_minFilterParam;
	m_magFilterParam = other.m_magFilterParam;
	return *this;
}

Texture Texture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, m_id);
	return *this;
}
void Texture::UnBind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::SetWrapS(int param)
{
	if (param == GL_REPEAT ||
		param == GL_MIRRORED_REPEAT ||
		param == GL_CLAMP_TO_EDGE ||
		param == GL_CLAMP_TO_BORDER)
	{
		Bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, param);
		m_wrapTParam = param;
		UnBind();
	}
	else
	{
		cout << "Texture: Invalid param" << endl;
	}
}
void Texture::SetWrapT(int param)
{
	if (param == GL_REPEAT ||
		param == GL_MIRRORED_REPEAT ||
		param == GL_CLAMP_TO_EDGE ||
		param == GL_CLAMP_TO_BORDER)
	{
		Bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, param);
		m_wrapSParam = param;
		UnBind();
	}
	else
	{
		cout << "Texture: Invalid param" << endl;
	}
}
void Texture::SetMinFilter(int param)
{
	if (param == GL_NEAREST ||
		param == GL_LINEAR ||
		param == GL_NEAREST_MIPMAP_NEAREST ||
		param == GL_LINEAR_MIPMAP_NEAREST ||
		param == GL_LINEAR_MIPMAP_LINEAR ||
		param == GL_NEAREST_MIPMAP_LINEAR)
	{
		Bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, param);
		m_minFilterParam = param;
		UnBind();
	}
	else
	{
		cout << "Texture: Invalid param" << endl;
	}
}
void Texture::SetMagFilter(int param)
{
	if (param == GL_NEAREST ||
		param == GL_LINEAR ||
		param == GL_NEAREST_MIPMAP_NEAREST ||
		param == GL_LINEAR_MIPMAP_NEAREST ||
		param == GL_LINEAR_MIPMAP_LINEAR ||
		param == GL_NEAREST_MIPMAP_LINEAR)
	{
		Bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, param);
		m_magFilterParam = param;
		UnBind();
	}
	else
	{
		cout << "Texture: Invalid param" << endl;
	}
}

Texture Texture::CreateFromFile(const char* file)
{
	if (m_id)
		Destroy();
	m_strName = file;
	glGenTextures(1, &m_id);
	Bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_wrapSParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_wrapTParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minFilterParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magFilterParam);

	std::vector<unsigned char> pixels;
	lodepng::decode(pixels, m_width, m_height, std::string(file), LodePNGColorType::LCT_RGBA, 8);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
	glGenerateMipmap(GL_TEXTURE_2D);
	UnBind();
	return *this;
}