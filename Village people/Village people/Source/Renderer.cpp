#include "Renderer.h"
#include <GLEW\glew.h>
#include <GLM\gtc\matrix_transform.hpp>

Renderer::Renderer()
{
}
Renderer::~Renderer()
{
}

void Renderer::Load(float a_left, float a_right, float a_top, float a_bottom)
{
	float vertices[] = {
		0.5f,  0.5f, 1, 1,  // top right
		0.5f, -0.5f, 1, 0,  // bottom right
		-0.5f, -0.5f, 0, 0,  // bottom left
		-0.5f,  0.5f, 0, 1   // top left 
	};

	unsigned int indices[] =
	{
		0, 1, 3, 1, 2, 3
	};

	glGenVertexArrays(1, &m_vao);
	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ebo);

	glBindVertexArray(m_vao);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	m_projectionMatrix = glm::ortho(a_left, a_right, a_bottom, a_top, 0.0f, 1000.0f);


	const char* vertexShader =
		"#version 330 core\n\
layout (location=0) in vec2 vertexPosition;\n\
layout (location=1) in vec2 texCoords;\n\
uniform mat4 MVP;\
out vec2 fragTexCoords;\
void main()\n\
{\n\
	gl_Position = MVP * vec4(vertexPosition, 0.0f, 1.0f);\n\
	fragTexCoords = texCoords;\n\
}";

	const char* fragmentShader =
		"#version 330 core\n\
in vec2 fragTexCoords;\n\
out vec4 colour;\n\
uniform sampler2D texture1;\
void main()\n\
{\n\
	colour = texture(texture1, fragTexCoords);\n\
}";
	m_defaultShader.CreateFromString(vertexShader, fragmentShader);
	m_defaultShader.SetUniform("MVP");
}

void Renderer::Unload()
{
	m_defaultShader.Destroy();
	if (m_ebo)
		glDeleteBuffers(1, &m_ebo);
	if (m_vbo)
		glDeleteBuffers(1, &m_vbo);
	if (m_vao)
		glDeleteVertexArrays(1, &m_vao);
}

void Renderer::Render(Shader* a_shader, Texture& a_texture, const glm::vec2 & a_position, const glm::vec2 & a_size, float a_rotation)
{
	Shader* shader = a_shader;
	if (shader == nullptr)
		shader = &m_defaultShader;
	shader->Use();
	a_texture.Bind();

	glm::mat4 model;
	model = glm::translate(glm::mat4(), glm::vec3(a_position, 0.0f));
	model = glm::scale(model, glm::vec3(a_size, 1.0f));
	model = glm::rotate(model, a_rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	model = m_projectionMatrix * model; //View matrix
	shader->SendMat4("MVP", model);

	DrawSquareVerts();
}

void Renderer::DrawSquareVerts()
{
	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}