#include "MemoryLeakDetector.h"
#include "Game.h"

int main()
{
	Game* app = new Game("AI", 1920, 1080);
	app->Run(1280, 720);
	delete app;

    return 0;
}