#include "Shader.h"
#include <GLEW\glew.h>
#include <GLM\gtc\type_ptr.hpp>
#include <fstream>
#include <sstream>
#include <iostream>

Shader::Shader() :
	m_id(0)
{
}
Shader::~Shader()
{
}
void Shader::Destroy()
{
	if (m_id)
		glDeleteProgram(m_id);
}

Shader Shader::CreateFromString(const char* a_vertex, const char* a_fragment)
{
	int success = 0;
	char infoLog[512];
	unsigned int vertexID = 0;
	unsigned int fragmentID = 0;

	//Vertex Shader
	if (a_vertex)
	{
		vertexID = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexID, 1, &a_vertex, 0);
		glCompileShader(vertexID);

		glGetShaderiv(vertexID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertexID, 512, 0, infoLog);
			std::cout << "Vertex: Could not create shader (" << infoLog << ")" << std::endl;
		}
	}
	//Fragment Shader
	if (a_fragment)
	{
		fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentID, 1, &a_fragment, 0);
		glCompileShader(fragmentID);
		glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragmentID, 512, 0, infoLog);
			std::cout << "Fragment: Could not create shader (" << infoLog << ")" << std::endl;
		}
	}

	//program
	m_id = glCreateProgram();
	if (vertexID)
		glAttachShader(m_id, vertexID);
	if (fragmentID)
		glAttachShader(m_id, fragmentID);

	glLinkProgram(m_id);
	glGetProgramiv(m_id, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(m_id, 512, 0, infoLog);
		std::cout << "Program: Could not link shaders (" << infoLog << ")" << std::endl;
	}

	//dump
	if (vertexID)
		glDeleteShader(vertexID);
	if (fragmentID)
		glDeleteShader(fragmentID);
	assert(success && "Shader creation did not succeed");
	return *this;
}
Shader Shader::CreateFromFile(const char* a_vertex, const char* a_fragment)
{
	std::string vertexString, fragmentString;

	if (a_vertex)
	{
		std::ifstream vertexFile;
		std::stringstream vertexStream;

		vertexFile.open(a_vertex);
		assert(vertexFile.good() && "Shader::Vertex: Could not read file");

		vertexStream << vertexFile.rdbuf();
		vertexString = vertexStream.str();

		vertexFile.close();
	}
	if (a_fragment)
	{
		std::ifstream fragmentFile;
		std::stringstream fragmentStream;

		fragmentFile.open(a_fragment);
		assert(fragmentFile.good() && "Shader::Fragment: Could not read file");

		fragmentStream << fragmentFile.rdbuf();
		fragmentString = fragmentStream.str();

		fragmentFile.close();
	}

	const char* vertexCode = nullptr;
	const char* fragmentCode = nullptr;
	if (vertexString != "")
		vertexCode = vertexString.c_str();
	if (fragmentString != "")
		fragmentCode = fragmentString.c_str();

	CreateFromString(vertexCode, fragmentCode);
	return *this;
}

void Shader::Use()
{
	assert(m_id);
	glUseProgram(m_id);
}

void Shader::SendInt(const char* a_location, int a_value)
{
	assert(m_id);
	glUniform1i(m_uniforms[a_location], a_value);
}
void Shader::SendUint(const char* a_location, unsigned int a_value)
{
	assert(m_id);
	glUniform1ui(m_uniforms[a_location], a_value);
}
void Shader::SendFloat(const const char* a_location, float a_value)
{
	assert(m_id);
	glUniform1f(m_uniforms[a_location], a_value);
}
void Shader::SendBool(const char* a_location, bool a_value)
{
	assert(m_id);
	glUniform1i(m_uniforms[a_location], a_value);
}
void Shader::SendMat4(const const char* a_location, const glm::mat4& a_value)
{
	assert(m_id);
	glUniformMatrix4fv(m_uniforms[a_location], 1, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendMat3(const char* a_location, const glm::mat3& a_value)
{
	assert(m_id);
	glUniformMatrix3fv(m_uniforms[a_location], 1, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendMat2(const char* a_location, const glm::mat2& a_value)
{
	assert(m_id);
	glUniformMatrix2fv(m_uniforms[a_location], 1, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendVec4(const char* a_location, const glm::vec4& a_value)
{
	assert(m_id);
	glUniform4fv(m_uniforms[a_location], 1, glm::value_ptr(a_value));
}
void Shader::SendVec3(const char* a_location, const glm::vec3& a_value)
{
	assert(m_id);
	glUniform3fv(m_uniforms[a_location], 1, glm::value_ptr(a_value));
}
void Shader::SendVec2(const char* a_location, const glm::vec2& a_value)
{
	assert(m_id);
	glUniform2fv(m_uniforms[a_location], 1, glm::value_ptr(a_value));
}

void Shader::SetUniform(const char * a_name)
{
	m_uniforms[a_name] = glGetUniformLocation(m_id, a_name);
}