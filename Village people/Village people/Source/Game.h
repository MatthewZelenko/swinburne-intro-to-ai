#pragma once
#include <Application.h>
#include "Shader.h"
#include "Texture.h"
#include "Renderer.h"
class Game : public Application
{
public:
	Game(std::string a_title, int a_gameWidth, int a_gameHeight);
	~Game();


protected:
	virtual void Load() override;
	virtual void Unload() override;
	virtual void ProcessInput(float a_deltaTime) override;
	virtual void FixedUpdate(float a_deltaTime) override;
	virtual void Update(float a_deltaTime) override;
	virtual void Render(float a_deltaTime) override;


private:
	int m_gameWidth, m_gameHeight;
	Texture m_texture;
	Renderer m_renderer;
};