#pragma once

class Texture
{
public:
	Texture();
	Texture(const Texture& a_other);
	~Texture();
	void Destroy();

	Texture& operator=(const Texture& a_other);

	Texture CreateFromFile(const char* a_file);
	Texture Bind();
	void UnBind();

	void SetWrapS(int a_param);
	void SetWrapT(int a_param);
	void SetMinFilter(int a_param);
	void SetMagFilter(int a_param);

	unsigned int GetID() { return m_id; }
	int GetHeight() { return m_height; }
	int GetWidth() { return m_width; }
	const char* GetName() { return m_strName; }

protected:
	const char* m_strName;
	unsigned int m_id;
	unsigned int m_width;
	unsigned int m_height;

	int m_wrapTParam, m_wrapSParam, m_minFilterParam, m_magFilterParam;
};