#define GLEW_STATIC

#include "Application.h"
#include "Debugger.h"
#include <GLEW\glew.h>
#include <GLFW\glfw3.h>
#include <GLFW\glfw3.h>
#include "Time.h"
#include "Input.h"

Application::Application(std::string a_title) : m_title(a_title), m_isRunning(false)
{
}
Application::~Application()
{
}

void Application::Run(int a_windowWidth, int a_windowHeight)
{
	SystemLoad(a_windowWidth, a_windowHeight);
	Load();
	while (m_isRunning)
	{
		SystemUpdate();
		double deltaTime = Time::Get().GetDeltaTime();
		ProcessInput(deltaTime);
		//FixedUpdate();
		Update(deltaTime);
		Render(deltaTime);
	}
	Unload();
	SystemUnload();
}

void Application::SystemLoad(int a_width, int a_height)
{
	glfwSetErrorCallback([](int a_error, const char* a_description) {
		LOG(a_description);
	});
	int error = glfwInit();
	if (error == GLFW_FALSE)
	{
		LOG("Could not initialise GLFW");
		return;
	}


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	if (!m_window.Create(m_title, a_width, a_height))
	{
		LOG("Could not create window");
		return;
	}
	m_window.MakeCurrent();

	glewExperimental = true;
	error = glewInit();
	if (error != GLEW_OK)
	{
		LOG("Could not initialise GLEW: %s", glewGetErrorString(error));
		return;
	}
	
	glClearColor(0.1f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	Time::Get();
	Input::Get().Init(m_window.GetWindow());
	m_isRunning = true;
}

void Application::SystemUpdate()
{
	Time::Get().Update();
	if (m_window.ShouldClose())
		m_isRunning = false;

	Input::Get().Update();
	glfwPollEvents();
}

void Application::SystemUnload()
{
	m_window.Terminate();
	glfwTerminate();
}