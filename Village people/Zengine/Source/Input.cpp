#include "Input.h"
#include <GLFW\glfw3.h>
#include <assert.h>

Input::Input()
{
	for (size_t i = 0; i < KEYCODE_TOTAL; i++)
	{
		m_keys[i] = KEYSTATE_UP;
	}
	m_currentPressedKeyIndex = 0;
}
Input::~Input()
{
}
void Input::KeyCallback(GLFWwindow * a_window, int a_key, int a_scancode, int a_action, int a_mods)
{
	Input& input = Input::Get();
	if (a_action >= GLFW_PRESS)
	{
		if (input.m_keys[a_key] == KEYSTATE_UP || input.m_keys[a_key] == KEYSTATE_RELEASED)
		{
			input.m_keys[a_key] = KEYSTATE_PRESSED;
			input.m_pressedKeys[input.m_currentPressedKeyIndex] = a_key;
			input.m_currentPressedKeyIndex++;
		}
	}
	else if (a_action == GLFW_RELEASE)
	{
		input.m_keys[a_key] = KEYSTATE_RELEASED;
		input.m_pressedKeys[input.m_currentPressedKeyIndex] = a_key;
		input.m_currentPressedKeyIndex++;
	}
}
Input & Input::Get()
{
	static Input instance;
	return instance;
}

void Input::Init(GLFWwindow* a_window)
{
	glfwSetKeyCallback(a_window, KeyCallback);
}

bool Input::IsKey(KeyCode a_key, KeyState a_keyState)
{
	assert(a_key >= 0 && a_key < KEYCODE_TOTAL);
	return m_keys[a_key] & a_keyState;
}
bool Input::IsKey(KeyCode a_key, int a_keyState)
{
	assert(a_key >= 0 && a_key < KEYCODE_TOTAL);
	return m_keys[a_key] & a_keyState;
}

void Input::Update()
{
	if (m_currentPressedKeyIndex != 0)
	{
		for (size_t i = 0; i < m_currentPressedKeyIndex; i++)
		{
			if (m_keys[m_pressedKeys[i]] == KEYSTATE_PRESSED)
				m_keys[m_pressedKeys[i]] = KEYSTATE_DOWN;
			else if (m_keys[m_pressedKeys[i]] == KEYSTATE_RELEASED)
				m_keys[m_pressedKeys[i]] = KEYSTATE_UP;
		}
		m_currentPressedKeyIndex = 0;
	}
}