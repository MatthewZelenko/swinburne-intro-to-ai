#include "Window.h"
#include <GLFW\glfw3.h>
#include <assert.h>

Window::Window()
{
}


Window::~Window()
{
}

bool Window::Create(std::string a_title, int a_width, int a_height)
{
	m_window = glfwCreateWindow(a_width, a_height, a_title.c_str(), nullptr, nullptr);
	return m_window != nullptr;
}

void Window::MakeCurrent()
{
	assert(m_window != nullptr && "Can not make null window current.");
	glfwMakeContextCurrent(m_window);
}

bool Window::ShouldClose()
{
	return glfwWindowShouldClose(m_window);
}

void Window::SwapBuffers()
{
	glfwSwapBuffers(m_window);
}

void Window::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::Terminate()
{
	if (m_window != nullptr)
	{
		glfwDestroyWindow(m_window);
		m_window = nullptr;
	}
}