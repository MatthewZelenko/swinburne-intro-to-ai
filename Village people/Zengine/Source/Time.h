#pragma once
class Time
{
public:
	static Time& Get();

	void Update();
	double GetDeltaTime() { return m_deltaTime; }
	double GetTime() { return m_time; }

private:
	Time();
	~Time();

	double m_deltaTime;
	double m_time;
	__int64 m_lastTime;
	double m_secondsPerCount;
};