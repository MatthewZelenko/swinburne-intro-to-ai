#pragma once
#include <string>
class GLFWwindow;
class Window
{
public:
	Window();
	~Window();

	bool Create(std::string a_title, int a_width, int a_height);
	void Terminate();

	void MakeCurrent();
	bool ShouldClose();
	void SwapBuffers();
	void Clear();

	GLFWwindow* GetWindow() { return m_window; }

private:
	GLFWwindow* m_window;

};