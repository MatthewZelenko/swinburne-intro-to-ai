#pragma once
#include <string>

#define LOG(a_message, ...) Debugger::Log(__TIME__, __FILE__, __FUNCTION__, __LINE__, a_message, __VA_ARGS__)
	

static class Debugger
{
public:
	static void Log(const char* a_time, const char* a_file, const char* a_function, int a_line,  const char* a_message, ...);

};