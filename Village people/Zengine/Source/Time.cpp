#include "Time.h"
#include <Windows.h>


Time::Time()
{
	m_deltaTime = 0;
	m_time = 0;
	m_lastTime = 0;

	QueryPerformanceCounter((LARGE_INTEGER*)&m_lastTime);

	__int64 timeFrequency = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&timeFrequency);

	m_secondsPerCount = 1.0 / timeFrequency;
}


Time::~Time()
{
}

Time& Time::Get()
{
	static Time instance;
	return instance;
}

void Time::Update()
{
	__int64 currentTime = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);
	m_deltaTime = (currentTime - m_lastTime) * m_secondsPerCount;
	m_lastTime = currentTime;
	m_time += m_deltaTime;
}