#include "Debugger.h"
#include <stdarg.h>

#ifndef DEBUG
void Debugger::Log(const char * a_time, const char * a_file, const char * a_function, int a_line, const char * a_message, ...)
{
	std::string message = "|";
	message += a_time;
	message += "|\t";
	message += a_file;
	message += "::";
	message += a_function;
	message += "(";
	message += a_line;
	message += "): \n\t";
	message += a_message;
	message += "\n";

	va_list lst;
	va_start(lst, a_message);
	std::vprintf(message.c_str(), lst);
	va_end(lst);
}
#else
void Debugger::Log(const char * a_time, const char * a_file, const char * a_function, int a_line, const char * a_message, ...)
{
	//std::string message = a_time;
	//message += "\t";
	//message += a_file;
	//message += "::";
	//message += a_function;
	//message += "(";
	//message += a_line;
	//message += "): ";
	//message += a_message;
	//
	//va_list lst;
	//va_start(lst, a_message);
	//std::printf(message.c_str(), lst);
	//va_end(lst);
}
#endif //!_DEBUG