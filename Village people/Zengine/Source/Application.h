#pragma once
#include <string>
#include "Window.h"

class Application
{
public:
	Application(std::string a_title);
	~Application();

	void Run(int a_windowWidth, int a_windowHeight);

protected:
	virtual void Load() = 0;
	virtual void Unload() = 0;

	virtual void ProcessInput(float a_deltaTime) = 0;
	virtual void FixedUpdate(float a_deltaTime) = 0;
	virtual void Update(float a_deltaTime) = 0;
	virtual void Render(float a_deltaTime) = 0;


	void Quit() { m_isRunning = false; }

	Window m_window;

private:
	void SystemLoad(int a_width, int a_height);
	void SystemUpdate();
	void SystemUnload();


	std::string m_title;
	bool m_isRunning;
};