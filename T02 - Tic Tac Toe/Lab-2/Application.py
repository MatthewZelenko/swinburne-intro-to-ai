from Agent import Agent

class Application(object):
    
    m_isRunning = True
    m_players = []
    m_currentPlayer = 0
    m_board = []
    #Top Row, Top Left Diagonal, Left Column, Middle Column, Right Column, Top Right Diagonal, Middle Row, Bottom Row
    m_winningMoves = [[0, 1, 2], [0, 4, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [2, 4, 6], [3, 4, 5], [6, 7, 8]]

    def __init__(this):
        this.m_players.append(Agent(Agent.AgentType.BASICAI, 1))
        this.m_players.append(Agent(Agent.AgentType.BASICAI, 2))
        for x in range(9):
            this.m_board.append(0)

    def Run(this):
        #Get number of players
        numOfPlayers = int(input("How many Players(0 - 2): "));
        numOfPlayers = max(0, min(numOfPlayers, 2))
        for x in range(numOfPlayers):
            this.m_players[x].m_agentType = Agent.AgentType.PLAYER

        if numOfPlayers != 2:
            count = 0
            for x in this.m_players:
                if x.m_agentType == Agent.AgentType.BASICAI:
                    res = input("Would you like AI(%d): to be advanced? " % count)
                    if res == "Y" or res == "y":
                        x.m_agentType = Agent.AgentType.ADVANCEDAI
                count += 1

        while this.m_isRunning:
            this.ProcessInput()
            this.Update()
            this.Render()

    def ProcessInput(this):
            print("\nPlayer %d(%s) you are up:" % (this.m_currentPlayer, ("x" if this.m_currentPlayer == 0 else "O")))
            #Get answer from player/AI
            res = this.m_players[this.m_currentPlayer].GetAnswer(this.m_board)            
            this.invalidMove = False
            this.m_board[res] = this.m_currentPlayer + 1
            #Has the player one using this one move?
            hasWon = this.HasWon();
            if hasWon == 1:
                #Someone one
                this.PrintBoard()
                print("Player %d(%s) Has WON!" % (this.m_currentPlayer, ("x" if this.m_currentPlayer == 0 else "y")))
                this.PlayAgain()
            elif hasWon == 0:
                #No one has one yet
                this.m_currentPlayer += 1
                this.m_currentPlayer %= 2
            else:
                #There is a tie
                this.PrintBoard()
                print("It's a TIE!")
                this.PlayAgain()

    def Update(this):
        pass
    def Render(this):
        this.PrintBoard()

    '''Asks Players if they want to play again
       Resets the board if Y
       Exits games if N'''
    def PlayAgain(this):
        if(input("Play Again? ") == "Y"):
            this.m_isRunning = True
            this.m_currentPlayer = 0
            for x in range(9):
                this.m_board[x] = 0
        else:
            this.m_isRunning = False

    '''Returns -1 if board is a tie
       Returns 1 if someone has won
       Returns 0 if nonone has won'''
    def HasWon(this):
        for x in this.m_winningMoves:
            if this.m_board[x[0]] == this.m_currentPlayer + 1 and this.m_board[x[1]] == this.m_currentPlayer + 1 and this.m_board[x[2]] == this.m_currentPlayer + 1:
                return 1
        for x in this.m_board:
            if x == 0:
                return 0
        return -1

    '''Prints out the board'''
    def PrintBoard(this):
        for y in range(3):
            print("| ", end="")
            for x in range(3):
                res = this.m_board[(y * 3 + x)];
                if res == 1:
                    res = "X"
                elif res == 2:
                    res = "O"
                else:
                    res = str(y * 3 + x)

                print("%s | " % res, end="")
            print("")