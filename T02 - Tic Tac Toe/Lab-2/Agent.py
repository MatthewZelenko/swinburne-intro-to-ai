from random import randrange
from enum import Enum
class Agent(object):
    """description of class"""
    
    class AgentType(Enum):
        PLAYER = 0,
        BASICAI = 1,
        ADVANCEDAI = 2

    m_agentType = None
    m_playerIndex = 0
        
    def __init__(this, a_agentType: AgentType, a_playerIndex : int):
        this.m_agentType = a_agentType
        this.m_playerIndex = a_playerIndex

    def GetAnswer(this, a_listMoves: []):
        if this.m_agentType == Agent.AgentType.PLAYER:
            return this.PlayerAnswer(a_listMoves)
        elif this.m_agentType == Agent.AgentType.BASICAI:
            return this.BasicAgentAnswer(a_listMoves)
        elif this.m_agentType == Agent.AgentType.ADVANCEDAI:
            return this.AdvancedAgentAnswer(a_listMoves)
        
    
    def PlayerAnswer(this, a_listMoves: []):
        #Keep looping if moves are invalid
        while(True):
            res = input("Where would you like to place your marker? ")            
            try:
               res = int(res)
            except:
                res = -1

            if res < 0 or res >= len(a_listMoves) or a_listMoves[res] != 0:
                print("Invalid move: please enter one of the following: ")
                for x in range(9):
                    if a_listMoves[x] == 0:
                        print("\t%d" % x, end="")
                        print("")
            else:
                return res

    def BasicAgentAnswer(this, a_listMoves: []):
        goodMoves = []
        for x in range(len(a_listMoves)):
            if a_listMoves[x] == 0:
                goodMoves.append(x)
        return goodMoves[randrange(0, len(goodMoves))]

    def AdvancedAgentAnswer(this, a_listMoves: []):
        bestMove = -1
        score = 0
        for y in range(3):
            for x in range(3):
                currentIndex = y * 3 + x
                newScore = 0
                if a_listMoves[currentIndex] == 0:
                    #Check surrounding Tiles
                    #Handle enemy winning
                    newScore = this.CalculateScore(x, y, a_listMoves)
                if newScore > score:
                    score = newScore
                    bestMove = currentIndex
                    if bestMove >= 9000000:
                        return bestMove
        return bestMove

    def CalculateScore(this, a_currentIndexX : int, a_currentIndexY: int, a_listMoves: []):
        score = 0

        if a_currentIndexX == 1 and a_currentIndexY == 1:
            for y in range(3):
                for x in range(3):
                    if y == 0 or (y == 1 and x == 0):
                        if a_listMoves[x] == 0:
                            #check other side
                            value = a_listMoves[(2 - y) * 3 + (2 - x)]
                            if value == 0:
                                score += 100
                            elif value == this.m_playerIndex:
                                #winning move
                                return 10000000
                            else:
                                score -= 100
        else:
            for y in range(a_currentIndexY - 1, a_currentIndexY + 2):
                if y < 0 or y > 2:
                    continue
                for x in range(a_currentIndexX - 1, a_currentIndexX + 2):    
                    if x < 0 or x > 2 or (x == a_currentIndexX and y == a_currentIndexY):
                        continue

                    if a_currentIndexX == 1 or a_currentIndexY == 1:
                        #Only check hor and ver not diagonal
                        if x != a_currentIndexX and y != a_currentIndexY:
                            continue

                    currentIndex = y * 3 + x
                    otherSideX = a_currentIndexX
                    otherSideY = a_currentIndexY

                    if a_listMoves[currentIndex] == this.m_playerIndex:
                        if a_currentIndexX != x:
                            otherSideX = 2 - otherSideX
                        if a_currentIndexY != y:
                            otherSideY = 2 - otherSideY
                        otherSideValue = a_listMoves[otherSideY * 3 + otherSideX]
                        if otherSideValue == this.m_playerIndex:
                            #Winning move
                            return 10000000
                        elif otherSideValue == 0:
                            #Free spot
                            score += 200
                        else:
                            #Enemy spot, can't win
                            score -= 100
                    elif a_listMoves[currentIndex] == 0:
                        #Open spot
                        score += 10
                    else:
                        #Enemy spot, can't win
                        score -= 100
        return score