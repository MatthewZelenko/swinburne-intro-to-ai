from Application import Application


def Main():
    application = Application()
    application.Run()


if __name__ == "__main__":
    Main()