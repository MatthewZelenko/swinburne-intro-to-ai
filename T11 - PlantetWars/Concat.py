'''Retaliation, '''
class BlackDeath(object):
    def __init__(self):
        self.elapse = 0
        self.first_planet = None

    def update(self, gameinfo):
        if not (gameinfo.my_planets and gameinfo.not_my_planets):
            return

        if self.first_planet == None:
            self.first_planet = max(gameinfo.my_planets.values(), key=lambda p: p.num_ships).id
        
        for x in gameinfo.my_fleets.values():
            if x.dest.num_ships > x.num_ships and (x.dest.owner_id != 0 and x.dest.owner_id != x.owner_id):
                dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.num_ships)
                if dest.num_ships > x.num_ships:
                    gameinfo.fleet_order(x, x.src, x.num_ships)
                else:
                    gameinfo.fleet_order(x, dest, x.num_ships)

        self.elapse += 1


        if self.elapse >= 10:
            self.elapse -= 10
            for x in gameinfo.my_planets.values():
                if x.id == self.first_planet and (len(gameinfo.my_planets) > 1 or len(gameinfo.my_fleets) > 0):
                    dest = min(gameinfo.my_planets.values(), key=lambda p: p.num_ships)
                    if dest.id != x.id:
                        gameinfo.planet_order(x, dest, int(x.num_ships * 0.2))
                else:
                    dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.distance_to(x))
                    gameinfo.planet_order(x, dest, int(x.num_ships * 0.75))






class ChickenPox(object):    
    def __init__(self):
        self.elapse = 0

    def update(self, gameinfo):
        if not (gameinfo.my_planets and gameinfo.not_my_planets):
            return
        self.elapse += 1
        if self.elapse >= 10:
            self.elapse -= 10
            for x in gameinfo.my_planets.values():
                dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.distance_to(x))
                gameinfo.planet_order(x, dest, int(x.num_ships * 0.75))