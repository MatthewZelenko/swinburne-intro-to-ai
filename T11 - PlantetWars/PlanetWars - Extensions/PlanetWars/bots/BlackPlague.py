class BlackPlague(object):

    def update(self, gameinfo):
        if gameinfo.my_fleets:
            return

        if gameinfo.my_planets and gameinfo.not_my_planets:
            n_planets_not_attacked = list(gameinfo.not_my_planets.values())

            if len(n_planets_not_attacked) == 1:
                for m_id, m_planet in gameinfo.my_planets.items():
                    if m_planet.num_ships >= 100:
                        gameinfo.planet_order(m_planet, n_planets_not_attacked[0], int(m_planet.num_ships * 0.75))
            else:
                for m_id, m_planet in gameinfo.my_planets.items():
                    if not n_planets_not_attacked:
                        return

                    index = 0

                    smallestMagnitude = self.distanceSqrd(n_planets_not_attacked[0], m_planet)

                    for i in range(1, len(n_planets_not_attacked)):
                        newSmallestMagnitude = self.distanceSqrd(n_planets_not_attacked[i], m_planet)
                        if m_planet.num_ships >= 20 and n_planets_not_attacked[i].num_ships <= int(m_planet.num_ships * 0.75) and newSmallestMagnitude < smallestMagnitude:
                            smallestMagnitude = newSmallestMagnitude
                            index = i
                    if m_planet.num_ships >= 20 and n_planets_not_attacked[index].num_ships <= int(m_planet.num_ships * 0.75):
                        gameinfo.planet_order(m_planet, n_planets_not_attacked[index], int(m_planet.num_ships * 0.75))
                        n_planets_not_attacked.pop(index)

    def distanceSqrd(self, a_planet1, a_planet2):
        closestX = abs(a_planet1.x - a_planet2.x)
        closestY = abs(a_planet1.y - a_planet2.y)
        return closestX * closestX + closestY * closestY