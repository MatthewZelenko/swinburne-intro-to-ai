class ChickenPox(object):    
    def __init__(self):
        self.elapse = 0

    def update(self, gameinfo):
        if not (gameinfo.my_planets and gameinfo.not_my_planets):
            return
        self.elapse += 1
        if self.elapse >= 10:
            self.elapse -= 10
            for x in gameinfo.my_planets.values():
                dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.distance_to(x))
                gameinfo.planet_order(x, dest, int(x.num_ships * 0.75))