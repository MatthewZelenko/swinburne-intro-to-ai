#----------------------------AGENT------------------------------
'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward cwoodward@swin.edu.au

'''
from weapon import Weapon
from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians, radians, sqrt
from random import random, randrange, uniform
from path import Path


class Agent(object):

    WEAPONS = {
        KEY._1: Weapon('bow_and_arrow', 2, 4, 2.0, 4), # speed, size, firerate, accuracy
        KEY._2: Weapon('sniper', 4, 1, 1.5, 0),
        KEY._3: Weapon('rocket', 1, 10, 3, 8)
    }

    def __init__(self, id=0, world=None, scale=10.0, weapon=WEAPONS[KEY._1]):
        # keep a reference to the world object
        self.world = world
        self.weapon = weapon
        # where am i and where am i going?  random start pos
        dir = radians(random() * 360)
        self.pos = Vector2D(randrange(scale, world.cx - scale), randrange(scale, world.cy - scale))
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        
        self.id = id

        self.color = (uniform(0, 1), uniform(0, 1), uniform(0, 1), 1.0)

        # data for drawing this agent
        self.vehicle_shape = [Point2D(0, 1),
            Point2D(-0.866025,  0.5),
            Point2D(-0.866025, -0.5),
            Point2D(0, -1),
            Point2D(0.866025, -0.5),
            Point2D(0.866025, 0.5)]

        # debug draw info?
        self.show_info = False

    def update(self, delta):
        ''' update vehicle position and orientation '''  
        target = None
        if len(self.world.targets) != 0:
            target = self.world.targets[0]

        if target:
            targetVel = target.dir * target.speed * 0.5

            a = (targetVel.x * targetVel.x) + (targetVel.y * targetVel.y) - (self.weapon.speed * self.weapon.speed)
            b = 2 * (targetVel.x * (target.pos.x - self.pos.x) + targetVel.y * (target.pos.y - self.pos.y))
            c = ((target.pos.x - self.pos.x) * (target.pos.x - self.pos.x)) + ((target.pos.y - self.pos.y) * (target.pos.y - self.pos.y))
 
            disc = b * b - (4 * a * c)

            if disc >= 0:
                t1 = (-1 * b + sqrt(disc)) / (2 * a)
                t2 = (-1 * b - sqrt(disc)) / (2 * a)
                t = max(t1,t2)
                aimX = (targetVel.x * t) + target.pos.x
                aimY = target.pos.y + (targetVel.y * t)

                self.heading = (Vector2D(aimX, aimY) - self.pos).normalise()
                self.side = Vector2D(self.heading.y, -self.heading.x)
                self.weapon.update(delta)

                bullet = self.weapon.shoot(self.pos.copy(), self.heading, delta)
                if bullet:
                    self.world.bullets.append(bullet)




    def render(self, color=None):
        # draw the ship
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        egi.green_pen()
        egi.closed_shape(pts, filled=True)
        egi.white_pen()
        egi.closed_shape(pts, filled=False)

        #Draw line of sight
        egi.red_pen()
        egi.line(pos1=self.pos, pos2=self.pos + (self.heading * 1000.0))

        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos + self.vel * s, self.pos + (self.force + self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos + (self.force + self.vel) * s, 5)
#----------------------------AGENT------------------------------




#----------------------------MAIN------------------------------
'''Autonomous Agent Movement: Seek, Arrive and Flee

Created for COS30002 AI for Games, Lab 05
By Clinton Woodward cwoodward@swin.edu.au

'''
from graphics import egi, KEY
from pyglet import window, clock
from pyglet.gl import *

from vector2d import Vector2D
from world import World
from agent import Agent 
from target import Target
from random import randrange, uniform

def on_mouse_press(x, y, button, modifiers):
    pass;


def on_key_press(symbol, modifiers):
    if symbol == KEY.P:
        world.paused = not world.paused
    elif symbol in Agent.WEAPONS:
        for agent in world.agents:
            agent.weapon = Agent.WEAPONS[symbol]
    # Toggle debug force line info on the agent
    elif symbol == KEY.I:
        for agent in world.agents:
            agent.show_info = not agent.show_info

    elif symbol == KEY.T:
        r = randrange(5, 20)
        i = randrange(0, 8)
        if i == 0:
            minPoint = Vector2D(r, r)
            maxPoint = Vector2D(world.cx - r, r)
        elif i == 1:
            minPoint = Vector2D(world.cx - r, r)
            maxPoint = Vector2D(r, r)
        elif i == 2:
            minPoint = Vector2D(r, r)
            maxPoint = Vector2D(r, world.cy - r)
        elif i == 3:
            minPoint = Vector2D(r, world.cy - r)
            maxPoint = Vector2D(r, r)
        elif i == 4:
            minPoint = Vector2D(world.cx - r, world.cy - r)
            maxPoint = Vector2D(world.cx - r, r)
        elif i == 5:
            minPoint = Vector2D(world.cx - r, r)
            maxPoint = Vector2D(world.cx - r, world.cy - r)
        elif i == 6:
            minPoint = Vector2D(world.cx - r, world.cy - r)
            maxPoint = Vector2D(r, world.cy - r)
        elif i == 7:
            minPoint = Vector2D(r, world.cy - r)
            maxPoint = Vector2D(world.cx - r, world.cy - r)

        world.targets.append(Target(minPoint, maxPoint, r, uniform(1, 2)))

def on_resize(cx, cy):
    pass;


if __name__ == '__main__':

    # create a pyglet window and set glOptions
    win = window.Window(width=500, height=500, vsync=True, resizable=True)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # needed so that egi knows where to draw
    egi.InitWithPyglet(win)
    # prep the fps display
    fps_display = clock.ClockDisplay()
    # register key and mouse event handlers
    win.push_handlers(on_key_press)
    win.push_handlers(on_mouse_press)
    win.push_handlers(on_resize)

    # create a world for agents
    world = World(500, 500)
    # unpause the world ready for movement
    world.paused = False

    #Attacker
    world.agents.append(Agent(len(world.agents), world))

    while not win.has_exit:
        win.dispatch_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # show nice FPS bottom right (default)
        delta = clock.tick()
        world.update(delta)
        world.render()
        fps_display.draw()
        # swap the double buffer
        win.flip()
#----------------------------MAIN------------------------------




#----------------------------TARGET------------------------------
from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path


class Target(object):

    def __init__(self, minPos, maxPos, size, speed = 2.0):
        self.size = size
        self.minPos = minPos
        self.maxPos = maxPos
        self.speed = speed

        self.dir = self.maxPos - self.minPos
        self.lenOfPoints = self.dir.length()
        self.dir.normalise()
        self.pos = self.minPos + self.dir * uniform(0, self.lenOfPoints)
        self.movingForward = True

    def update(self, delta):
        self.pos += self.dir * self.speed
        if self.movingForward:
            if (self.pos - self.maxPos).length() <= self.speed:
                self.movingForward = False;
                self.pos = self.maxPos.copy()
                self.dir = -self.dir
        else:
            if (self.pos - self.minPos).length() <= self.speed:
                self.movingForward = True
                self.pos = self.minPos.copy()
                self.dir = -self.dir
        
    def render(self):
        egi.red_pen()
        egi.circle(self.pos, self.size, filled=True)
        egi.circle(self.pos, self.size * 0.5, filled=True)
        egi.circle(self.pos, self.size * 0.25, filled=True)
#----------------------------TARGET------------------------------




#----------------------------WEAPON------------------------------
from vector2d import Vector2D
from bullet import Bullet
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path


class Weapon(object):

    def __init__(self, name, speed, size, shootDelay, accuracy):
        self.name = name
        self.speed = speed
        self.size = size
        self.shootDelay = 1.0
        self.shootDelayElapse = 0.0
        self.accuracy = accuracy

    def update(self, delta):
        self.shootDelayElapse = min(self.shootDelay, self.shootDelayElapse + delta)

    def shoot(self, pos, direction, delta):
        if self.shootDelayElapse >= self.shootDelay:
            self.shootDelayElapse -= self.shootDelay

            angleInRadians = radians(uniform(-self.accuracy, self.accuracy))

            targetDirection = Vector2D(direction.x * cos(angleInRadians) - direction.y * sin(angleInRadians),
                                           direction.x * sin(angleInRadians) + direction.y * cos(angleInRadians))

            return Bullet(pos, targetDirection, self.speed, self.size)
        return None
#----------------------------WEAPON------------------------------




#----------------------------WORLD------------------------------
'''A 2d world that supports agents with steering behaviour

Created for COS30002 AI for Games by Clinton Woodward cwoodward@swin.edu.au

'''

from vector2d import Vector2D
from matrix33 import Matrix33
from graphics import egi
from random import random, randrange, uniform
from Obstacle import Obstacle

class World(object):
    def __init__(self, cx, cy):
        self.cx = cx
        self.cy = cy
        self.agents = []
        self.targets = []
        self.bullets = []
        self.paused = True
        self.show_info = True

    def update(self, delta):
        if not self.paused:
            for agent in self.agents:
                agent.update(delta)
                            
            for target in self.targets:
                target.update(delta)

            for bullet in self.bullets:
                bullet.update(delta)

            for b in reversed(self.bullets):
                b.update(delta)
                if (b.pos.x <= b.size or b.pos.x >= self.cx or b.pos.y <= b.size or b.pos.y >= self.cy):
                    self.bullets.remove(b)
                    continue
                for t in reversed(self.targets):
                    if b.Collides(t.pos, t.size):
                        self.targets.remove(t)
                        self.bullets.remove(b)
                        break
                    

    def render(self):
        for agent in self.agents:
            agent.render()
            
        for target in self.targets:
            target.render()
            
        for bullet in self.bullets:
            bullet.render()

        #if self.show_info:
        #    infotext = ', '.join(set(agent.mode for agent in self.agents))
        #    egi.white_pen()
        #    egi.text_at_pos(0, 0, infotext)


    def wrap_around(self, pos):
        ''' Treat world as a toroidal space. Updates parameter object pos '''
        max_x, max_y = self.cx, self.cy
        if pos.x > max_x:
            pos.x = pos.x - max_x
        elif pos.x < 0:
            pos.x = max_x - pos.x
        if pos.y > max_y:
            pos.y = pos.y - max_y
        elif pos.y < 0:
            pos.y = max_y - pos.y

    def transform_points(self, points, pos, forward, side, scale):
        ''' Transform the given list of points, using the provided position,
            direction and scale, to object world space. '''
        # make a copy of original points (so we don't trash them)
        wld_pts = [pt.copy() for pt in points]
        # create a transformation matrix to perform the operations
        mat = Matrix33()
        # scale,
        mat.scale_update(scale.x, scale.y)
        # rotate
        mat.rotate_by_vectors_update(forward, side)
        # and translate
        mat.translate_update(pos.x, pos.y)
        # now transform all the points (vertices)
        mat.transform_vector2d_list(wld_pts)
        # done
        return wld_pts
    
    def transform_point(self, point, pos, forward, side):
        ''' Transform the given single point, using the provided position, and direction (forward and side unit vectors), to object world space. '''
        # make a copy of the original point (so we don't trash it)
        wld_pt = point.copy()
        # create a transformation matrix to perform the operations
        mat = Matrix33()
        # rotate
        mat.rotate_by_vectors_update(forward, side)
        # and translate
        mat.translate_update(pos.x, pos.y)
        # now transform the point (in place)
        mat.transform_vector2d(wld_pt)
        # done
        return wld_pt
#----------------------------WORLD------------------------------