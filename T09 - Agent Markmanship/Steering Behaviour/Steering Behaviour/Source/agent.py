'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward cwoodward@swin.edu.au

'''
from weapon import Weapon
from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians, radians, sqrt
from random import random, randrange, uniform
from path import Path


class Agent(object):

    WEAPONS = {
        KEY._1: Weapon('bow_and_arrow', 2, 4, 2.0, 4), # speed, size, firerate, accuracy
        KEY._2: Weapon('sniper', 4, 1, 1.5, 0),
        KEY._3: Weapon('rocket', 1, 10, 3, 8)
    }

    def __init__(self, id=0, world=None, scale=10.0, weapon=WEAPONS[KEY._1]):
        # keep a reference to the world object
        self.world = world
        self.weapon = weapon
        # where am i and where am i going?  random start pos
        dir = radians(random() * 360)
        self.pos = Vector2D(randrange(scale, world.cx - scale), randrange(scale, world.cy - scale))
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        
        self.id = id

        self.color = (uniform(0, 1), uniform(0, 1), uniform(0, 1), 1.0)

        # data for drawing this agent
        self.vehicle_shape = [Point2D(0, 1),
            Point2D(-0.866025,  0.5),
            Point2D(-0.866025, -0.5),
            Point2D(0, -1),
            Point2D(0.866025, -0.5),
            Point2D(0.866025, 0.5)]

        # debug draw info?
        self.show_info = False

    def update(self, delta):
        ''' update vehicle position and orientation '''  
        target = None
        if len(self.world.targets) != 0:
            target = self.world.targets[0]

        if target:
            targetVel = target.dir * target.speed * 0.5

            a = (targetVel.x * targetVel.x) + (targetVel.y * targetVel.y) - (self.weapon.speed * self.weapon.speed)
            b = 2 * (targetVel.x * (target.pos.x - self.pos.x) + targetVel.y * (target.pos.y - self.pos.y))
            c = ((target.pos.x - self.pos.x) * (target.pos.x - self.pos.x)) + ((target.pos.y - self.pos.y) * (target.pos.y - self.pos.y))
 
            disc = b * b - (4 * a * c)

            if disc >= 0:
                t1 = (-1 * b + sqrt(disc)) / (2 * a)
                t2 = (-1 * b - sqrt(disc)) / (2 * a)
                t = max(t1,t2)
                aimX = (targetVel.x * t) + target.pos.x
                aimY = target.pos.y + (targetVel.y * t)

                self.heading = (Vector2D(aimX, aimY) - self.pos).normalise()
                self.side = Vector2D(self.heading.y, -self.heading.x)
                self.weapon.update(delta)

                bullet = self.weapon.shoot(self.pos.copy(), self.heading, delta)
                if bullet:
                    self.world.bullets.append(bullet)




    def render(self, color=None):
        # draw the ship
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        egi.green_pen()
        egi.closed_shape(pts, filled=True)
        egi.white_pen()
        egi.closed_shape(pts, filled=False)

        #Draw line of sight
        egi.red_pen()
        egi.line(pos1=self.pos, pos2=self.pos + (self.heading * 1000.0))

        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos + self.vel * s, self.pos + (self.force + self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos + (self.force + self.vel) * s, 5)