from vector2d import Vector2D
from bullet import Bullet
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path


class Weapon(object):

    def __init__(self, name, speed, size, shootDelay, accuracy):
        self.name = name
        self.speed = speed
        self.size = size
        self.shootDelay = 1.0
        self.shootDelayElapse = 0.0
        self.accuracy = accuracy

    def update(self, delta):
        self.shootDelayElapse = min(self.shootDelay, self.shootDelayElapse + delta)

    def shoot(self, pos, direction, delta):
        if self.shootDelayElapse >= self.shootDelay:
            self.shootDelayElapse -= self.shootDelay

            angleInRadians = radians(uniform(-self.accuracy, self.accuracy))

            targetDirection = Vector2D(direction.x * cos(angleInRadians) - direction.y * sin(angleInRadians),
                                           direction.x * sin(angleInRadians) + direction.y * cos(angleInRadians))

            return Bullet(pos, targetDirection, self.speed, self.size)
        return None