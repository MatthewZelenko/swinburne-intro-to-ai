from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path


class Bullet(object):

    def __init__(self, pos, direction, speed, size):
        self.speed = speed
        self.size = size
        self.direction = direction
        self.pos = pos

    def update(self, delta):
        self.pos += self.direction * self.speed

    def render(self):
        egi.aqua_pen()
        egi.circle(self.pos, self.size, True)

    def Collides(self, pos, radius):
        if (self.pos - pos).length_sq() <= (radius + self.size) * (radius + self.size):
            return True
        return False