﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Camera m_camera;
    public Map m_map;
    public float m_movementThreshold = 0.1f;
    public float m_movementSpeed = 128.0f;
    void Start()
    {
        m_camera = GetComponent<Camera>();
    }
    void Update()
    {
        Vector2 mousePos = m_camera.ScreenToViewportPoint(Input.mousePosition);
        Debug.Log(mousePos);

        if (mousePos.x <= m_movementThreshold)
        {
            transform.Translate(new Vector3(-m_movementSpeed, 0.0f) * Time.deltaTime);
        }
        else if (mousePos.x >= 1.0f - m_movementThreshold)
        {
            transform.Translate(new Vector3(m_movementSpeed, 0.0f) * Time.deltaTime);
        }

        if (mousePos.y <= m_movementThreshold)
        {
            transform.Translate(new Vector3(0.0f, -m_movementSpeed) * Time.deltaTime);
        }
        else if (mousePos.y >= 1.0f - m_movementThreshold)
        {
            transform.Translate(new Vector3(0.0f, m_movementSpeed) * Time.deltaTime);
        }

        if (transform.position.x <= 0.0f)
            transform.position.Set(0.0f, transform.position.y, transform.position.z);


        if (transform.position.y <= 0.0f)
            transform.position.Set(transform.position.x, 0.0f, transform.position.z);
    }
}