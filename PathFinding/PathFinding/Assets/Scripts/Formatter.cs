﻿using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class Formatter
{
    static bool m_isDebuggable = false;
    public static bool Debuggable
    {
        get { return m_isDebuggable; }
        set { m_isDebuggable = value; }
    }

    public static void SerializeJSONToFile(string a_path, string a_name, object a_object)
    {
        if (a_object == null || a_path == null || a_name == null)
            return;
        FileStream file = null;
        JsonSerializer ser = new JsonSerializer();
        ser.TypeNameHandling = TypeNameHandling.All;
        JsonWriter writer = null;
        StreamWriter stream = null;
        try
        {
            Directory.CreateDirectory(a_path);
            file = new FileStream(a_path + a_name, FileMode.Create);
            if (Debuggable)
            {
                stream = new StreamWriter(file);
                writer = new JsonTextWriter(stream) { Formatting = Formatting.Indented };
                ser.Serialize(writer, a_object, a_object.GetType());
            }
            else
            {
                writer = new BsonWriter(file);
                ser.Serialize(writer, a_object);
            }
        }
        catch (Exception e)
        {
            throw new Exception("Could not serialize: " + a_path, e);
        }
        finally
        {
            if (writer != null)
                writer.Close();
            if (stream != null)
                stream.Close();
            if (file != null)
                file.Close();
        }
    }
    public static byte[] SerializeJSONToSource(object a_object)
    {
        byte[] data = null;
        if (a_object == null)
            return data;
        MemoryStream memStr = new MemoryStream();
        JsonSerializer ser = new JsonSerializer();
        ser.TypeNameHandling = TypeNameHandling.All;
        JsonWriter writer = null;
        StreamWriter stream = null;
        try
        {
            if (Debuggable)
            {
                stream = new StreamWriter(memStr);
                writer = new JsonTextWriter(stream) { Formatting = Formatting.Indented };
            }
            else
            {
                writer = new BsonWriter(memStr);
            }
            ser.Serialize(writer, a_object, a_object.GetType());
            data = memStr.GetBuffer();
        }
        catch (Exception e)
        {
            throw new Exception("Could not serialize: ", e);
        }
        finally
        {
            if (memStr != null)
                memStr.Close();
            if (stream != null)
                stream.Close();
            if (writer != null)
                writer.Close();
        }
        return data;
    }

    public static T DeserializeJSON<T>(string a_path)
    {
        T retObject = default(T);
        if (!File.Exists(a_path))
            return retObject;

        FileStream file = null;
        JsonSerializer ser = new JsonSerializer();
        ser.TypeNameHandling = TypeNameHandling.All;
        JsonReader reader = null;
        StreamReader stream = null;

        try
        {
            file = new FileStream(a_path, FileMode.Open, FileAccess.Read, FileShare.Read);

            if (Debuggable)
            {
                stream = new StreamReader(file);
                reader = new JsonTextReader(stream);
            }
            else
            {
                reader = new BsonReader(file);
            }
            retObject = ser.Deserialize<T>(reader);
        }
        catch (Exception e)
        {
            throw new Exception("Could not deserialize: " + a_path, e);
        }
        finally
        {
            if (reader != null)
                reader.Close();
            if (stream != null)
                stream.Close();
            if (file != null)
                file.Close();
        }
        return retObject;
    }
    public static T DeserializeJSONFromSource<T>(byte[] a_source)
    {
        T retObject = default(T);
        if (a_source == null)
            return retObject;

        JsonSerializer ser = new JsonSerializer();
        ser.TypeNameHandling = TypeNameHandling.All;
        StreamReader strReader = null;
        JsonReader reader = null;
        MemoryStream stream = null;

        try
        {
            stream = new MemoryStream(a_source);
            if (Debuggable)
            {
                strReader = new StreamReader(stream);
                reader = new JsonTextReader(strReader);
            }
            else
            {
                reader = new BsonReader(stream);
            }
            retObject = ser.Deserialize<T>(reader);
        }
        catch (Exception e)
        {
            throw new Exception("Could not deserialize: From Source", e);
        }
        finally
        {
            if (strReader != null)
                strReader.Close();
            if (stream != null)
                stream.Close();
            if (reader != null)
                reader.Close();
        }
        return retObject;
    }

    public static T DeserializeJSONToken<T>(string a_filePath, string a_propertyPath, JsonToken a_token)
    {
        T retObject = default(T);
        if (!File.Exists(a_filePath))
            return retObject;

        FileStream file = null;
        JsonSerializer ser = new JsonSerializer();
        ser.TypeNameHandling = TypeNameHandling.All;
        JsonReader reader = null;
        StreamReader stream = null;

        try
        {
            file = new FileStream(a_filePath, FileMode.Open, FileAccess.Read, FileShare.Read);

            if (Debuggable)
            {
                stream = new StreamReader(file);
                reader = new JsonTextReader(stream);
            }
            else
            {
                reader = new BsonReader(file);
            }

            while (reader.Read())
            {
                if (reader.TokenType == a_token && (reader.Path as string) == a_propertyPath)
                {
                    retObject = ser.Deserialize<T>(reader);
                    break;
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception("Could not deserialize: " + a_filePath, e);
        }
        finally
        {
            if (reader != null)
                reader.Close();
            if (stream != null)
                stream.Close();
            if (file != null)
                file.Close();
        }
        return retObject;
    }

    public static T DeserializeJSONFromSourceToken<T>(byte[] a_source, string a_propertyPath, JsonToken a_token)
    {
        T retObject = default(T);
        if (a_source == null)
            return retObject;

        JsonSerializer ser = new JsonSerializer();
        ser.TypeNameHandling = TypeNameHandling.All;
        StreamReader strReader = null;
        JsonReader reader = null;
        MemoryStream stream = null;

        try
        {
            stream = new MemoryStream(a_source);
            if (Debuggable)
            {
                strReader = new StreamReader(stream);
                reader = new JsonTextReader(strReader);
            }
            else
            {
                reader = new BsonReader(stream);
            }
            while (reader.Read())
            {
                if (reader.TokenType == a_token && (reader.Path as string) == a_propertyPath)
                {
                    retObject = ser.Deserialize<T>(reader);
                    break;
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception("Could not deserialize: From Source", e);
        }
        finally
        {
            if (strReader != null)
                strReader.Close();
            if (stream != null)
                stream.Close();
            if (reader != null)
                reader.Close();
        }
        return retObject;
    }
}