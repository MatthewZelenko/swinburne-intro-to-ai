﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Bson;

public class TileData
{
    public int[] m_tiles;
}

public class Map : MonoBehaviour
{
    public Sprite m_spriteSheet;
    public int m_spriteSheetCols, m_spriteSheetRows, m_selectedSpriteSheetIndex;

    public int m_mapCols, m_mapRows, m_tileSize;

    MeshRenderer m_meshRenderer;
    MeshFilter m_meshFilter;

    Rect m_mapRect;

    TileData m_data;

    void Start()
    {
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_meshFilter = GetComponent<MeshFilter>();
        if (m_spriteSheet != null)
        {
            CreateMesh();
            m_meshRenderer.sharedMaterial.mainTexture = m_spriteSheet.texture;
        }
        else
            CreateMesh(true);

    }
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            if (m_mapRect.Contains(mousePosition))
            {
                int xIndex = (int)(mousePosition.x / m_tileSize);
                int yIndex = (int)(mousePosition.y / m_tileSize);
                SetUV(xIndex, yIndex, m_selectedSpriteSheetIndex);
            }
        }
    }

    void CreateMesh(bool a_randomColours = false)
    {
        m_meshFilter.sharedMesh = new Mesh();

        int verticesCount = m_mapCols * m_mapRows * 4;
        Vector3[] vertices = new Vector3[verticesCount];
        Vector3[] normals = new Vector3[verticesCount]; // norm coords
        Vector2[] uvs = new Vector2[verticesCount]; //unit coords
        Color[] colours = new Color[verticesCount]; //Colours


        int indiciesCount = m_mapCols * m_mapRows * 2/*Two triangles per col_row*/ * 3/*Vertices per triangle*/;
        int[] trianglesIndicies = new int[indiciesCount];



        verticesCount /= 4;

        m_data = new TileData
        {
            m_tiles = new int[verticesCount]
        };


        for (int i = 0; i < verticesCount; i++)
        {
            m_data.m_tiles[i] = 0;


            int x = i % m_mapCols;
            int y = i / m_mapCols;
            vertices[i * 4 + 0] = new Vector3(x * m_tileSize, y * m_tileSize, 0);
            vertices[i * 4 + 1] = new Vector3((x + 1) * m_tileSize, y * m_tileSize, 0);
            vertices[i * 4 + 2] = new Vector3((x + 1) * m_tileSize, (y + 1) * m_tileSize, 0);
            vertices[i * 4 + 3] = new Vector3(x * m_tileSize, (y + 1) * m_tileSize, 0);
            //NORMALS
            normals[i * 4 + 0] = Vector3.forward;
            normals[i * 4 + 1] = Vector3.forward;
            normals[i * 4 + 2] = Vector3.forward;
            normals[i * 4 + 3] = Vector3.forward;

            //UVS
            uvs[i * 4 + 0] = new Vector2(0, 0);
            uvs[i * 4 + 1] = new Vector2(0, 0);
            uvs[i * 4 + 2] = new Vector2(0, 0);
            uvs[i * 4 + 3] = new Vector2(0, 0);

            if (a_randomColours)
            {
                Color col = Random.ColorHSV();

                colours[i * 4 + 0] = col;
                colours[i * 4 + 1] = col;
                colours[i * 4 + 2] = col;
                colours[i * 4 + 3] = col;
            }
            else
            {
                colours[i * 4 + 0] = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                colours[i * 4 + 1] = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                colours[i * 4 + 2] = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                colours[i * 4 + 3] = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            }
            trianglesIndicies[i * 6 + 0] = i * 4;
            trianglesIndicies[i * 6 + 1] = i * 4 + 3;
            trianglesIndicies[i * 6 + 2] = i * 4 + 2;
            trianglesIndicies[i * 6 + 3] = i * 4 + 2;
            trianglesIndicies[i * 6 + 4] = i * 4 + 1;
            trianglesIndicies[i * 6 + 5] = i * 4;
        }


        m_meshFilter.sharedMesh.vertices = vertices;
        m_meshFilter.sharedMesh.normals = normals;
        m_meshFilter.sharedMesh.uv = uvs;
        m_meshFilter.sharedMesh.colors = colours;
        m_meshFilter.sharedMesh.triangles = trianglesIndicies;

        m_mapRect.size = new Vector2(m_mapCols * m_tileSize, m_mapRows * m_tileSize);
    }
    private Vector2[] GetUVsFromSpriteSheet(int spriteIndex)
    {
        Vector2[] uvs = new Vector2[4];
        float x = 0;
        float y = 0;
        if (m_spriteSheetCols != 0 && m_spriteSheetRows != 0)
        {
            x = spriteIndex % m_spriteSheetCols;
            y = spriteIndex / m_spriteSheetCols;
            y = y % m_spriteSheetRows;

            uvs[0] = new Vector2(x / m_spriteSheetCols, y / m_spriteSheetRows);
            uvs[1] = new Vector2((x + 1) / m_spriteSheetCols, y / m_spriteSheetRows);
            uvs[2] = new Vector2((x + 1) / m_spriteSheetCols, (y + 1) / m_spriteSheetRows);
            uvs[3] = new Vector2(x / m_spriteSheetCols, (y + 1) / m_spriteSheetRows);
        }

        return uvs;
    }
    void SetUV(int a_xTileIndex, int a_yTileIndex, int a_spriteIndex)
    {
        Mesh mesh = m_meshFilter.sharedMesh;
        //TODO: if performace is crap, create vector2D[] Variable of uvs.
        Vector2[] uvs = mesh.uv;
        Vector2[] newUvs = GetUVsFromSpriteSheet(a_spriteIndex);

        int tileIndex = a_xTileIndex + a_yTileIndex * m_mapCols;

        int index = tileIndex * 6;
        uvs[mesh.triangles[index]] = newUvs[0];
        uvs[mesh.triangles[index + 1]] = newUvs[1];
        uvs[mesh.triangles[index + 2]] = newUvs[2];
        uvs[mesh.triangles[index + 4]] = newUvs[3];

        mesh.uv = uvs;

        m_data.m_tiles[tileIndex] = a_spriteIndex;
    }

    public void SaveMap()
    {
        Formatter.SerializeJSONToFile("Saved Maps/", "Map.bytes", m_data);
    }
    public void LoadMap()
    {
        m_data = Formatter.DeserializeJSON<TileData>("Saved Maps/Map.bytes");

        int currentSpriteIndex = -1;
        Vector2[] spriteSheetUVS = null;
        Vector2[] uvs = m_meshFilter.sharedMesh.uv;

        for (int i = 0; i < m_data.m_tiles.Length; i++)
        {
            if(currentSpriteIndex != m_data.m_tiles[i])
            {
                currentSpriteIndex = m_data.m_tiles[i];
                spriteSheetUVS = GetUVsFromSpriteSheet(currentSpriteIndex);
            }

            int index = i * 6;
            uvs[m_meshFilter.sharedMesh.triangles[index]] = spriteSheetUVS[0];
            uvs[m_meshFilter.sharedMesh.triangles[index + 1]] = spriteSheetUVS[1];
            uvs[m_meshFilter.sharedMesh.triangles[index + 2]] = spriteSheetUVS[2];
            uvs[m_meshFilter.sharedMesh.triangles[index + 4]] = spriteSheetUVS[3];
        }
        m_meshFilter.sharedMesh.uv = uvs;
    }
}