g_stamina = 100
g_health = 200

g_state = 'Attacking'


while g_state != 'Done':
  if g_state == 'Attacking':
    g_stamina -= 10
    print("You swing your sword for 20 damage")
    print("\tHealth: ", g_health)
    print("\tStamina: ", g_stamina)
    if g_stamina == 0:
      g_state = 'Defending'
  elif g_state == 'Defending':
    g_health -= 10
    g_stamina += 20;
    print("The enemy hits you for 20 damage");
    print("\tHealth: ", g_health)
    print("\tStamina: ", g_stamina)
    if g_health <= 50:
      g_state = 'Fleeing'
    elif g_stamina == 100:
      g_state = 'Attacking'
  elif g_state == 'Fleeing':
    print("You have fled")
    print("\tHealth: ", g_health)
    print("\tStamina: ", g_stamina)
    g_state = 'Done'
    